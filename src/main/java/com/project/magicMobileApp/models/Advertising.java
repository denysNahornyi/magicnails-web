package com.project.magicMobileApp.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by denys on 11.01.17.
 */
@Entity
@Table(name = "advertising")
public class Advertising {

    @Id
    @GeneratedValue
    @Column(name = "idAdvertising")
    private long id;

    @Column(name = "advertisingName")
    private String name;

    @Column(name = "advertisingText")
    private String text;

    @Column(name = "advertisingDate1")
    private Date date1;

    @Column(name = "advertisingDate2")
    private Date date2;

    @ManyToOne(optional = false)
    @JoinColumn (name = "advertisingCustomer")
    private Customer customer;

    public Advertising(String name, String text, Date date1, Date date2, Customer customer) {
        this.name = name;
        this.text = text;
        this.date1 = date1;
        this.date2 = date2;
        this.customer = customer;
    }

    public Advertising(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Advertising{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", text='" + text + '\'' +
                ", date1=" + date1 +
                ", date2=" + date2 +
                ", customer=" + customer +
                '}';
    }
}
