package com.project.magicMobileApp.models.ModelsForApp.MainModels;

import com.project.magicMobileApp.models.ModelsForApp.AdvertisingModel;
import com.project.magicMobileApp.models.ModelsForApp.ServiceModel;

import java.util.Date;
import java.util.List;

/**
 * Created by denys on 22.01.17.
 */
public class CustomerModel {

    String id, nameCustomer, surname, secondName, card, bonuces, wellnessAmount;
    Date birthday;
    List<ServiceModel> serviceList;
    List<AdvertisingModel> advertisingModels;

    public CustomerModel(String id, String nameCustomer, String surname, String secondName, Date birthday, String card, String bonuces, String wellnessAmount, List<ServiceModel> serviceList, List<AdvertisingModel> advertisingModels) {
        this.id = id;
        this.nameCustomer = nameCustomer;
        this.surname = surname;
        this.secondName = secondName;
        this.card = card;
        this.birthday = birthday;
        this.bonuces = bonuces;
        this.wellnessAmount = wellnessAmount;
        this.serviceList = serviceList;
        this.advertisingModels = advertisingModels;
    }

    public CustomerModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getBonuces() {
        return bonuces;
    }

    public void setBonuces(String bonuces) {
        this.bonuces = bonuces;
    }

    public String getWellnessAmount() {
        return wellnessAmount;
    }

    public void setWellnessAmount(String wellnessAmount) {
        this.wellnessAmount = wellnessAmount;
    }

    public List<ServiceModel> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ServiceModel> serviceList) {
        this.serviceList = serviceList;
    }

    public List<AdvertisingModel> getAdvertisingModels() {
        return advertisingModels;
    }

    public void setAdvertisingModels(List<AdvertisingModel> advertisingModels) {
        this.advertisingModels = advertisingModels;
    }
}
