package com.project.magicMobileApp.models.ModelsForApp;

/**
 * Created by denys on 21.01.17.
 */
public class PriceModel {
    String name, price;

    public PriceModel(){}

    public PriceModel(String name, String price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PriceModel " + "{" +
                "name='" + name + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
