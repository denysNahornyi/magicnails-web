package com.project.magicMobileApp.models.ModelsForApp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denys on 21.01.17.
 */
public class WellnessModel {

    List<String> prices;
    List<String> names;



    public WellnessModel() {
    }

    public WellnessModel( List<String> prices, List<String> names) {
        this.prices = prices;
        this.names = names;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<String> getPrices() {
        return prices;
    }

    public void setPrices(List<String> prices) {
        this.prices = prices;
    }

    @Override
    public String toString() {
        return "WellnessModel{" +
                "prices=" + prices +
                ", names=" + names +
                '}';
    }
}
