package com.project.magicMobileApp.models.ModelsForApp;

import java.util.List;

/**
 * Created by denys on 11.02.17.
 */
public class PriceTypeModel {
    String name;
    List<PriceModel> priceModels;
    public PriceTypeModel(){}

    public PriceTypeModel(String name, List<PriceModel> priceModels){
        this.name = name;
        this.priceModels = priceModels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PriceModel> getPriceModels() {
        return priceModels;
    }

    public void setPriceModels(List<PriceModel> priceModels) {
        this.priceModels = priceModels;
    }


    @Override
    public String toString() {
        return "PriceTypeModel{" +
                "name='" + name + '\'' +
                ", priceModels=" + priceModels +
                '}';
    }
}
