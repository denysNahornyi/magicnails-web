package com.project.magicMobileApp.models.ModelsForApp;

import java.util.Date;

/**
 * Created by denys on 15.01.17.
 */
public class ServiceModel {

    String id, date, type, master, price, rating;

    int serviceId;

    public ServiceModel() {}

    public ServiceModel(String date, String type, String master, String price, String id, String rating, int serviceId) {
        this.date = date;
        this.type = type;
        this.master = master;
        this.price = price;
        this.id = id;
        this.rating = rating;
        this.serviceId = serviceId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public String getType() {
        return type;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public String toString() {
        return "ServiceModel{" +
                "id='" + id + '\'' +
                ", date='" + date + '\'' +
                ", type='" + type + '\'' +
                ", master='" + master + '\'' +
                ", price='" + price + '\'' +
                ", rating='" + rating + '\'' +
                ", serviceId=" + serviceId +
                '}';
    }
}
