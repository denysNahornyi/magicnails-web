package com.project.magicMobileApp.models.ModelsForApp.MainModels;

import com.project.magicMobileApp.models.ModelsForApp.PriceTypeModel;
import com.project.magicMobileApp.models.ModelsForApp.WellnessModel;

import java.util.List;

/**
 * Created by denys on 22.01.17.
 */
public class SaloonModel {
    WellnessModel wellnessModel;
    List<PriceTypeModel> priceTypeModels;
//    List<PriceModel> priceModel;
//    List<PriceModel> priceModel2;
//    List<PriceModel> priceModel3;
    String mobileNumber1, mobileNumber2, insta, site1, site2;

//    public SaloonModel(WellnessModel wellnessModel, List<PriceModel> priceModel1, List<PriceModel> priceModel2, List<PriceModel> priceModel3, String mobileNumber1, String mobileNumber2, String insta, String site1, String site2) {
public SaloonModel(WellnessModel wellnessModel, List<PriceTypeModel> priceTypeModels, String mobileNumber1, String mobileNumber2, String insta, String site1, String site2) {
        this.wellnessModel = wellnessModel;
//        this.priceModel = priceModel1;
//        this.priceModel2 = priceModel2;
//        this.priceModel3 = priceModel3;
        this.priceTypeModels = priceTypeModels;
        this.mobileNumber1 = mobileNumber1;
        this.mobileNumber2 = mobileNumber2;
        this.insta = insta;
        this.site1 = site1;
        this.site2 = site2;
    }



    public SaloonModel(){}
//
//    public List<PriceModel> getPriceModel2() {
//        return priceModel2;
//    }
//
//    public void setPriceModel2(List<PriceModel> priceModel2) {
//        this.priceModel2 = priceModel2;
//    }
//
//    public List<PriceModel> getPriceModel3() {
//        return priceModel3;
//    }
//
//    public void setPriceModel3(List<PriceModel> priceModel3) {
//        this.priceModel3 = priceModel3;
//    }
//
//    public WellnessModel getWellnessModel() {
//        return wellnessModel;
//    }
//
//    public void setWellnessModel(WellnessModel wellnessModel) {
//        this.wellnessModel = wellnessModel;
//    }
//
//    public List<PriceModel> getPriceModel() {
//        return priceModel;
//    }
//
//    public void setPriceModel(List<PriceModel> priceModel) {
//        this.priceModel = priceModel;
//    }

    public String getMobileNumber1() {
        return mobileNumber1;
    }

    public void setMobileNumber1(String mobileNumber1) {
        this.mobileNumber1 = mobileNumber1;
    }

    public String getMobileNumber2() {
        return mobileNumber2;
    }

    public void setMobileNumber2(String mobileNumber2) {
        this.mobileNumber2 = mobileNumber2;
    }

    public String getInsta() {
        return insta;
    }

    public void setInsta(String insta) {
        this.insta = insta;
    }

    public String getSite1() {
        return site1;
    }

    public void setSite1(String site1) {
        this.site1 = site1;
    }

    public String getSite2() {
        return site2;
    }

    public void setSite2(String site2) {
        this.site2 = site2;
    }

    public WellnessModel getWellnessModel() {
        return wellnessModel;
    }

    public void setWellnessModel(WellnessModel wellnessModel) {
        this.wellnessModel = wellnessModel;
    }

    public List<PriceTypeModel> getPriceTypeModels() {
        return priceTypeModels;
    }

    public void setPriceTypeModels(List<PriceTypeModel> priceTypeModels) {
        this.priceTypeModels = priceTypeModels;
    }

    @Override
    public String toString() {
        return "SaloonModel{" +
                "wellnessModel=" + wellnessModel +
                ", priceTypeModels=" + priceTypeModels +
                ", mobileNumber1='" + mobileNumber1 + '\'' +
                ", mobileNumber2='" + mobileNumber2 + '\'' +
                ", insta='" + insta + '\'' +
                ", site1='" + site1 + '\'' +
                ", site2='" + site2 + '\'' +
                '}';
    }
}
