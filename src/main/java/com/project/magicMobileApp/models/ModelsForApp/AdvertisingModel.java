package com.project.magicMobileApp.models.ModelsForApp;

/**
 * Created by denys on 22.01.17.
 */
public class AdvertisingModel {

    String name, text, date1, date2;

    public AdvertisingModel(String name, String text, String date1, String date2) {
        this.name = name;
        this.text = text;
        this.date1 = date1;
        this.date2 = date2;
    }

    public AdvertisingModel(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }
}

