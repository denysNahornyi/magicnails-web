package com.project.magicMobileApp.models;

import javax.persistence.*;


@Entity
@Table(name = "masters")
public class Master {

    @Id
    @GeneratedValue
    @Column(name = "idMaster")
    private long id;

    @Column(name = "masterName")
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn (name = "masterPosition")
    private Position position;

    @Column(name = "masterSurname")
    private String surname;

    public Master(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Master(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Master{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
