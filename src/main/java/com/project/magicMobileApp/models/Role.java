package com.project.magicMobileApp.models;

import javax.persistence.*;

/**
 * Created by denys on 11.01.17.
 */


@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue
    @Column(name = "idRole")
    private long id;

    @Column(name = "nameRole")
    private String name;


    public Role(String name) {
        this.name = name;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Role(){

    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
//                ", customers=" + customers +
                '}';
    }
}
