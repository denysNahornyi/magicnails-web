package com.project.magicMobileApp.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by denys on 11.01.17.
 */
@Entity
@Table(name = "serviceHistory")
public class ServiceHistory {

    @Id
    @GeneratedValue
    @Column(name = "idSH")
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SHservice")
    private Services service;

    @Column(name = "SHdate")
    private Date date;

    @ManyToOne(optional = false)
    @JoinColumn (name = "SHcustomer")
    private Customer customer;

    @ManyToOne(optional = false )
    @JoinColumn(name = "SHmaster")
    private Master master;

    @Column(name = "SHbonuces")
    private double bonuces;

    @Column(name = "SHrating")
    private int rating = 0;

    public ServiceHistory(){
    }

    public ServiceHistory(Services service, Date date, Customer customer, Master master, double bonuces, int rating) {
        this.service = service;
        this.date = date;
        this.customer = customer;
        this.master = master;
        this.bonuces = bonuces;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Services getService() {
        return service;
    }

    public void setService(Services service) {
        this.service = service;
    }

    public Master getMaster() {
        return master;
    }

    public void setMaster(Master master) {
        this.master = master;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getBonuces() {
        return bonuces;
    }

    public void setBonuces(double bonuces) {
        this.bonuces = bonuces;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "ServiceHistory{" +
                "id=" + id +
                ", service=" + service +
                ", date=" + date +
                ", customer=" + customer +
                ", master=" + master +
                ", bonuces=" + bonuces +
                ", rating=" + rating +
                '}';
    }
}
