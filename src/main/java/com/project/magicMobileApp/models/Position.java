package com.project.magicMobileApp.models;

import javax.persistence.*;

/**
 * Created by denys on 11.02.17.
 */

@Entity
@Table(name = "position")
public class Position {

    @Id
    @GeneratedValue
    @Column(name = "idPosition")
    private long id;

    @Column(name = "namePosition")
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn (name = "positionAdress")
    private Adress adress;


    public Position(String name) {
        this.name = name;
    }

    public Position(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}








