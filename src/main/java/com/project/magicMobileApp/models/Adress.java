package com.project.magicMobileApp.models;

import javax.persistence.*;

/**
 * Created by denys on 12.02.17.
 */
@Entity
@Table(name = "adress")
public class Adress {


    @Id
    @GeneratedValue
    @Column(name = "idAdress")
    private long id;

    @Column(name = "nameAdress")
    private String name;

    public Adress(String name) {
        this.name = name;
    }

    public Adress(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}






