package com.project.magicMobileApp.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by denys on 15.02.17.
 */

@Entity
@Table(name = "appointment")
public class Appointment {
    @Id
    @GeneratedValue
    @Column(name = "idAppointment")
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn (name = "appointmentCustomer")
    private Customer customer;


    @Column(name = "timeAppointment")
    private Date time;

    public Appointment(){}

    public Appointment(Customer customer, Date time) {
        this.customer = customer;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", customer=" + customer +
                ", time=" + time +
                '}';
    }
}




















