package com.project.magicMobileApp.models;

/**
 * Created by denys on 11.01.17.
 */

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;


@Entity
@Table(name = "customer")
public class Customer implements Comparable<Customer> {



    @Id
    @GeneratedValue
    @Column(name = "idcustomer")
    private long id;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private Collection<ServiceHistory> serviceHistories = new ArrayList<ServiceHistory>();


    @Column(name = "customerLogin")
    private String login;

    @Column(name = "customerName" )
    private String name;

    @Column(name = "customerBirthday")
    private String date;

    @Column(name = "customerSurname")
    private String surname;

    @Column(name = "customerPatronymic")
    private String patronymic;

    @ManyToOne(optional = false)
    @JoinColumn (name = "customerRole")
    private Role role;

    @Column(name = "customerToken")
    private String token = "1";

    @Column(name = "customerPassword")
    private String password;

    @Column(name = "customerPhone")
    private String phone;

    @Column(name = "customerCard")
    private String card;

    @Column(name = "infoBonuses")
    private double bonuses;

    @Column(name = "customerWellnessinfo")
    private int wellness;

    @Lob
    @Column(name = "img")
    private String img;

    @Column(name = "lastUse")
    private Date lastUse;

    public Customer(){}

    public Customer(String login, String name, String surname, String patronymic, Role role, String password, String card, double bonuses, int wellness1, String token, String date, String phone) {
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.role = role;
        this.password = password;
        this.card = card;
        this.bonuses = bonuses;
        this.wellness = wellness1;
        this.token = token;
        this.date = date;
        this.phone = phone;
    }


    public Date getLastUse() {
        return lastUse;
    }

    public void setLastUse(Date lastUse) {
        this.lastUse = lastUse;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getBonuses() {
        return bonuses;
    }

    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public int getWellness() {
        return wellness;
    }

    public void setWellness(int wellness1) {
        this.wellness = wellness1;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", role=" + role +
                ", token='" + token + '\'' +
                ", password='" + password + '\'' +
                ", card=" + card +
                ", bonuses=" + bonuses +
                ", wellness=" + wellness +
                '}';
    }

    @Override
    public int compareTo(Customer o) {
        return Comparators.LOGIN.compare(this, o);
    }

    public static class Comparators {

        public static Comparator<Customer> LOGIN = new Comparator<Customer>() {
            @Override
            public int compare(Customer o1, Customer o2) { return o1.card.compareTo(o2.card);
            }
        };
    }
}

