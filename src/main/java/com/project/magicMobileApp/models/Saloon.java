package com.project.magicMobileApp.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by denys on 21.01.17.
 */
@Entity
@Table(name = "saloon")
public class Saloon {
    @Id
    @GeneratedValue
    @Column(name = "idSaloon")
    private long id;

//    @Column(name = "saloonName")
//    private String name;

    @Column(name = "saloonMnumber1")
    private String mobileNumber1;

    @Column(name = "saloonMnumber2")
    private String mobileNumber2;

    @Column(name = "saloonInsta")
    private String insta;

    @Column(name = "priceFor1Training")
    private String priceFor1Training;

    @Column(name = "priceForFirstMonth")
    private String priceForFirstMonth;

    @Column(name = "priceForSecondMonth")
    private String priceForSecondMonth;

    @Column(name = "testTraining")
    private String testTraining;

//    @Column(name = "saloonWEB1")
//    private String web1;
//
//    @Column(name = "saloonWEB2")
//    private String web2;

    @Column(name = "registration1")
    private String reg1;

    @Column(name = "registration2")
    private String reg2;

    @Column(name = "dbRemoveDate")
    private Date dbRemove;


    public Saloon( String mobileNumber1, String mobileNumber2, String insta, String priceFor1Training, String priceForFirstMonth, String priceForSecondMonth, String testTraining,  String reg1, String reg2) {
//        this.name = name;
        this.mobileNumber1 = mobileNumber1;
        this.mobileNumber2 = mobileNumber2;
        this.insta = insta;
        this.priceFor1Training = priceFor1Training;
        this.priceForFirstMonth = priceForFirstMonth;
        this.priceForSecondMonth = priceForSecondMonth;
        this.testTraining = testTraining;
//        this.web1 = web1;
//        this.web2 = web2;
        this.reg1 = reg1;
        this.reg2 = reg2;

    }

    public Saloon(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

    public String getMobileNumber1() {
        return mobileNumber1;
    }

    public void setMobileNumber1(String mobileNumber1) {
        this.mobileNumber1 = mobileNumber1;
    }

    public String getMobileNumber2() {
        return mobileNumber2;
    }

    public void setMobileNumber2(String mobileNumber2) {
        this.mobileNumber2 = mobileNumber2;
    }

    public String getInsta() {
        return insta;
    }

    public void setInsta(String insta) {
        this.insta = insta;
    }

    public String getPriceFor1Training() {
        return priceFor1Training;
    }

    public void setPriceFor1Training(String priceFor1Training) {
        this.priceFor1Training = priceFor1Training;
    }

    public String getPriceForFirstMonth() {
        return priceForFirstMonth;
    }

    public void setPriceForFirstMonth(String priceForFirstMonth) {
        this.priceForFirstMonth = priceForFirstMonth;
    }

    public String getPriceForSecondMonth() {
        return priceForSecondMonth;
    }

    public void setPriceForSecondMonth(String priceForSecondMonth) {
        this.priceForSecondMonth = priceForSecondMonth;
    }

    public String getTestTraining() {
        return testTraining;
    }

    public void setTestTraining(String testTraining) {
        this.testTraining = testTraining;
    }

//    public String getWeb1() {
//        return web1;
//    }
//
//    public void setWeb1(String web1) {
//        this.web1 = web1;
//    }
//
//    public String getWeb2() {
//        return web2;
//    }
//
//    public void setWeb2(String web2) {
//        this.web2 = web2;
//    }

    public String getReg1() {
        return reg1;
    }

    public void setReg1(String reg1) {
        this.reg1 = reg1;
    }

    public String getReg2() {
        return reg2;
    }

    public void setReg2(String reg2) {
        this.reg2 = reg2;
    }

    public Date getDbRemove() {
        return dbRemove;
    }

    public void setDbRemove(Date dbRemove) {
        this.dbRemove = dbRemove;
    }

    @Override
    public String toString() {
        return "Saloon{" +
                "id=" + id +
//                ", name='" + name + '\'' +
                ", mobileNumber1='" + mobileNumber1 + '\'' +
                ", mobileNumber2='" + mobileNumber2 + '\'' +
                ", insta='" + insta + '\'' +
                ", priceFor1Training='" + priceFor1Training + '\'' +
                ", priceForFirstMonth='" + priceForFirstMonth + '\'' +
                ", priceForSecondMonth='" + priceForSecondMonth + '\'' +
                ", testTraining='" + testTraining + '\'' +
//                ", web1='" + web1 + '\'' +
//                ", web2='" + web2 + '\'' +
                ", reg1='" + reg1 + '\'' +
                ", reg2='" + reg2 + '\'' +
                '}';
    }
}
