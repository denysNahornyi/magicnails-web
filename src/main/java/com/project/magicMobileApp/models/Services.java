package com.project.magicMobileApp.models;

import javax.persistence.*;
import javax.xml.ws.Service;

/**
 * Created by denys on 18.01.17.
 */
@Entity
@Table(name = "services")
public class Services {

    @Id
    @GeneratedValue
    @Column(name = "idServices")
    private long id;

    @Column(name = "servicesName")
    private String name;

    @Column(name = "servicesPrice")
    private int price;

    @Column(name = "servicesParam")
    private int param;


    public Services(String name, int price, int param) {
        this.name = name;
        this.price = price;
        this.param=param;
    }

    public Services(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getParam() {
        return param;
    }

    public void setParam(int param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return "Services{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", param=" + param +
                '}';
    }
}
