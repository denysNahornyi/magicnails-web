package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.*;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by denys on 11.02.17.
 */
@Transactional
public class PositionDAO {


    @PersistenceContext
    public EntityManager entityManager;


    public Position add(Position position){
        return entityManager.merge(position);
    }

    public Position get(int id){
        return  entityManager.find(Position.class, Long.valueOf(id));
    }

    public List<Position> getAll(){
        TypedQuery<Position> query = entityManager.createQuery(
                "select p from Position p where p.name!='-'", Position.class
        );
        List<Position> result = query.getResultList();
        return result;
    }

    public void delete(long id){
        entityManager.remove(entityManager.getReference(Position.class, id));
    }

    public List<Position> getAllById(long id){
        TypedQuery<Position> query = entityManager.createQuery(
                "select p from Position  p where p.adress.id = " + id, Position.class
        );
        List<Position> result = query.getResultList();
        return result;
    }
}
