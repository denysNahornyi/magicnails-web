package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.Saloon;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
public class SaloonDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public Saloon get(int id){
        return  entityManager.find(Saloon.class, Long.valueOf(id));
    }

    public Saloon add(Saloon saloon){
        return entityManager.merge(saloon);
    }
}