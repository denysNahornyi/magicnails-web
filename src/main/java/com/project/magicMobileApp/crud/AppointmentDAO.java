package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.Advertising;
import com.project.magicMobileApp.models.Appointment;
import com.project.magicMobileApp.models.Customer;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by denys on 15.02.17.
 */

@Transactional
public class AppointmentDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public Appointment add(Appointment appointment){
        return entityManager.merge(appointment);
    }

    public List<Appointment> getAll(){
        TypedQuery<Appointment> query = entityManager.createQuery(
                "select a from Appointment a", Appointment.class
        );
        List<Appointment> result = query.getResultList();
        return result;
    }

    public void delete(long id){
        entityManager.remove(entityManager.getReference(Appointment.class, id));
    }
}
