package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.Role;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
public class RoleDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public Role get(int id){
        return  entityManager.find(Role.class, Long.valueOf(id));
    }
}
