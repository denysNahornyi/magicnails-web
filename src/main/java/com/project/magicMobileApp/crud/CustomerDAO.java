package com.project.magicMobileApp.crud;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.project.magicMobileApp.models.Customer;

import java.awt.*;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;


@Transactional
public class CustomerDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public Customer get(long id){
        return entityManager.find(Customer.class, id);
    }

    public Customer add(Customer customer){
        return entityManager.merge(customer);
    }

    public void delete(long id){
        entityManager.remove(entityManager.getReference(Customer.class, id));
    }

    public Customer getByLogin(String login){
        Customer res = entityManager.createQuery(
        "select c from Customer c where c.login ='" + login+"'", Customer.class
        ).getSingleResult();
        return res;
    }

    public List<Customer> getAll(){
        TypedQuery<Customer> query = entityManager.createQuery(
                "select c from Customer c, Role r where c.role.id = r.id and  r.name = 'ROLE_USER'", Customer.class
        );
        List<Customer> result = query.getResultList();
        return result;
    }
}
