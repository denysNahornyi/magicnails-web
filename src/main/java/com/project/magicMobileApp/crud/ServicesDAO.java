package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.Services;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class ServicesDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public Services add(Services services){ return entityManager.merge(services);}

    public Services get(long id){return entityManager.find(Services.class, id);}

    public void delete(long id){
        entityManager.remove(entityManager.getReference(Services.class, id));
    }

    public List<Services> getAllM(){
        TypedQuery<Services> query = entityManager.createQuery(
                "select s from Services  s where s.price != 0 and s.param = 1 ", Services.class
        );
        List<Services> result = query.getResultList();
        return result;
    }

    public List<Services> getAll(){
        TypedQuery<Services> query = entityManager.createQuery(
                "select s from Services  s where s.price != 0", Services.class
        );
        List<Services> result = query.getResultList();
        return result;
    }

    public List<Services> getAllB(){
        TypedQuery<Services> query = entityManager.createQuery(
                "select s from Services  s where s.price != 0 and s.param = 0 ", Services.class
        );
        List<Services> result = query.getResultList();
        return result;
    }

    public List<Services> getAllR(){
        TypedQuery<Services> query = entityManager.createQuery(
                "select s from Services  s where s.price != 0 and s.param = 2 ", Services.class
        );
        List<Services> result = query.getResultList();
        return result;
    }

    public List<Services> getAllW(){
        TypedQuery<Services> query = entityManager.createQuery(
                "select s from Services s where s.price = 0", Services.class
        );
        List<Services> result = query.getResultList();
        return result;
    }
}
