package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.Adress;
import com.project.magicMobileApp.models.Position;
import com.project.magicMobileApp.models.ServiceHistory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by denys on 12.02.17.
 */
@Transactional
public class AdressDAO {

    @PersistenceContext
    public EntityManager entityManager;


    public Adress add(Adress adress){
        return entityManager.merge(adress);
    }

    public Adress get(int id){
        return  entityManager.find(Adress.class, Long.valueOf(id));
    }

    public List<Adress> getAll(){
        TypedQuery<Adress> query = entityManager.createQuery(
                "select a from Adress a where a.name !='-'", Adress.class
        );
        List<Adress> result = query.getResultList();
        return result;
    }

    public void delete(long id){
        entityManager.remove(entityManager.getReference(Adress.class, id));
    }




}
