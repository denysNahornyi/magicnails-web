package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.Master;
import com.project.magicMobileApp.models.Position;
import com.project.magicMobileApp.models.ServiceHistory;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class MasterDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public Master get(long id){return entityManager.find(Master.class, id);}

    public List<Master> getAll(){
        TypedQuery<Master> query = entityManager.createQuery(
                "select m from Master m where m.name != '-'", Master.class
        );
        List<Master> result = query.getResultList();
        return result;
    }

    public Master add(Master master){
        return entityManager.merge(master);
    }

    public void delete(long id){
        entityManager.remove(entityManager.getReference(Master.class, id));
    }


    public List<Master> getAllByPos(long id){
        TypedQuery<Master> query = entityManager.createQuery(
                "select m from Master  m where m.position.id = " + id, Master.class
        );
        List<Master> result = query.getResultList();
        return result;
    }
}
