package com.project.magicMobileApp.crud;

import com.project.magicMobileApp.models.Advertising;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Transactional
public class AdvertisingDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public Advertising add(Advertising advertising){
        return entityManager.merge(advertising);
    }

    public void delete(long id){
        entityManager.remove(entityManager.getReference(Advertising.class, id));
    }

    public List<Advertising> getAllbyId(int id){
        TypedQuery<Advertising> query = entityManager.createQuery(
                "select a from Advertising a where a.customer.id =" + id , Advertising.class
        );
        List<Advertising> result = query.getResultList();
        return result;
    }

    public List<Advertising> getAdvertisingByName(String name){
        TypedQuery<Advertising> query = entityManager.createQuery(
                "select a from Advertising a where a.name ='" + name +"'" , Advertising.class
        );
        List<Advertising> result = query.getResultList();
        return result;
    }


    public List<Advertising> deleteAdvertisings(Date date){
        TypedQuery<Advertising> query = entityManager.createQuery(
                "select a from Advertising a where a.date2 < current_date ", Advertising.class
        );
        List<Advertising> result = query.getResultList();
        return result;
    }

    //    public String getTextbyName(String name){
//        TypedQuery<Advertising> query = entityManager.createQuery(
//                "select a from Advertising a where a.name ='" + name +"'" , Advertising.class
//        );
//        List<Advertising> result = query.getResultList();
//        return result.get(0).getText();
//    }
    public HashSet<String> getAll(){
        TypedQuery<Advertising> query = entityManager.createQuery(
                "select a from Advertising a ", Advertising.class
        );
        List<Advertising> result = query.getResultList();
        List<String> x = new ArrayList<String>();
        for (Advertising a : result){
            x.add(a.getName());
        }
        HashSet<String> y = new HashSet(x);
        return y;
    }
}
