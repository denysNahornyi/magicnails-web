package com.project.magicMobileApp.crud;

import com.google.api.client.util.DateTime;
import com.project.magicMobileApp.models.ServiceHistory;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Transactional
public class ServiceHistoryDAO {
    @PersistenceContext
    public EntityManager entityManager;

    public ServiceHistory add(ServiceHistory serviceHistory){
        return entityManager.merge(serviceHistory);
    }

    public ServiceHistory get(long id){
        return entityManager.find(ServiceHistory.class, id);
    }

    public void delete(long id){
        entityManager.remove(entityManager.getReference(ServiceHistory.class, id));
    }

    public List<ServiceHistory> getById(long idCustomer){
        TypedQuery<ServiceHistory> query = entityManager.createQuery(
                "select sh from ServiceHistory sh where sh.customer.id =" + idCustomer,  ServiceHistory.class
        );
        List<ServiceHistory> result = query.getResultList();
        return result;
    }

    public List<ServiceHistory> getAll(){
        TypedQuery<ServiceHistory> query = entityManager.createQuery(
                "select s from ServiceHistory  s", ServiceHistory.class
        );
        List<ServiceHistory> result = query.getResultList();
        return result;
    }

    public List<ServiceHistory> getOld(String d){
        TypedQuery<ServiceHistory> query = entityManager.createQuery(

//               "select s from ServiceHistory  s where  s.date < current_date - interval '2' MONTH", ServiceHistory.class
                "select s from ServiceHistory  s where DATE(s.date) < '"+d+"'", ServiceHistory.class
        );
        List<ServiceHistory> result = query.getResultList();
        return result;
    }

    public List<ServiceHistory> getAllByDate(String d){
        TypedQuery<ServiceHistory> query = entityManager.createQuery(
                "select s from ServiceHistory  s where DATE(s.date) = '"+d+"'", ServiceHistory.class
        );
        List<ServiceHistory> result = query.getResultList();
        return result;
    }

    public List<ServiceHistory> getAllByMaster(long id){
        TypedQuery<ServiceHistory> query = entityManager.createQuery(
                "select s from ServiceHistory  s where s.master.id = " + id, ServiceHistory.class
        );
        List<ServiceHistory> result = query.getResultList();
        return result;
    }
    public List<ServiceHistory> getAllByMasterWithout0(long id){
        TypedQuery<ServiceHistory> query = entityManager.createQuery(
                "select s from ServiceHistory  s where s.rating != 0 and s.master.id = " + id, ServiceHistory.class
        );
        List<ServiceHistory> result = query.getResultList();
        return result;
    }
}
