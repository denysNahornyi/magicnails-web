package com.project.magicMobileApp.controllers;

import com.project.magicMobileApp.controllers.firebase.AndroidPushNotificationsService;
import com.project.magicMobileApp.controllers.firebase.FirebaseResponse;
import com.project.magicMobileApp.crud.*;
import com.project.magicMobileApp.models.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Controller
@RequestMapping("magicnail/")
public class WebController {

    @Autowired
    public CustomerDAO customerDAO;
    @Autowired
    public RoleDAO roleDAO;
    @Autowired
    public ServiceHistoryDAO serviceHistoryDAO;
    @Autowired
    public ServicesDAO servicesDAO;
    @Autowired
    public MasterDAO masterDAO;
    @Autowired
    public AdvertisingDAO advertisingDAO;
    @Autowired
    public SaloonDAO saloonDAO;
    @Autowired
    public PositionDAO positionDAO;
    @Autowired
    public AdressDAO adressDAO;
    @Autowired
    public AppointmentDAO appointmentDAO;
    @Autowired
    AndroidPushNotificationsService androidPushNotificationsService;



    @Scheduled(fixedDelay=900000) //15минут  900000
    public  void sh() throws ParseException, UnsupportedEncodingException {
        Date date = new Date();
        List<Appointment> appointments = appointmentDAO.getAll();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        for (Appointment a: appointments){
            long diff = a.getTime().getTime() - date.getTime();
            if(diff<14400000){   //2часа + 2 часа сервер
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(a.getTime());
                String t = String.format("Напоминаем о визите %s.%s на %s:%s в студию Magic Nails 0972702700; ул. Чавдар 2, оф. 197", calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
                notification("Вы не забыли о нас?:)", t,a.getCustomer());
                appointmentDAO.delete(a.getId());
            }
        }
    }

    @Scheduled(fixedDelay=21600000) //21600000
    public  void deleteOffers() {
        Date date = new Date();
        List<Advertising> advertisings = advertisingDAO.deleteAdvertisings(date);
        for (Advertising a: advertisings){
            advertisingDAO.delete(a.getId());
        }
    }


    @RequestMapping(value = "main", method = RequestMethod.GET)
    public  String main() {
        return "main";}

    @RequestMapping(value = "exit", method = RequestMethod.GET)
    public String exit() {
        SecurityContextHolder.clearContext();
        return "redirect:/";
    }

    @RequestMapping(value = "wellness", method = RequestMethod.GET)
    public  String wellness() {return "wellness";}

    @RequestMapping(value = "wellness/adding", method = RequestMethod.GET)
    public  String addW() {return "wellnessAdding";}

    @RequestMapping(value = "/wellness/adding", method = RequestMethod.POST)
    public String addW(HttpServletRequest request){
        String name = request.getParameter("name");
        Services services = new Services();
        services.setName(name);
        services.setPrice(0);
        services.setParam(1);
        servicesDAO.add(services);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "wellness/editing", method = RequestMethod.GET)
    public  String editW(Model model) {
        model.addAttribute("names", servicesDAO.getAllW());
        return "wellnessEditing";
    }

    @RequestMapping(value = "wellness/editing", method = RequestMethod.POST)
    public  String editW(HttpServletRequest request) {
        String id = request.getParameter("id");
        Services services = servicesDAO.get(Long.parseLong(id));
        String name = request.getParameter("name");
        services.setName(name);
        services.setPrice(0);
        servicesDAO.add(services);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "wellness/deleting/{id}", method = RequestMethod.GET)
    public  String deleteW(Model model) {
        model.addAttribute("wellness", servicesDAO.getAllW());
        return "wellnessDeleting";}

    @RequestMapping(value = "wellness/deleting/{id}", method = RequestMethod.POST)
    public  String deleteW(@PathVariable ("id") int id) {
        servicesDAO.delete(id);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "wellness/price", method = RequestMethod.GET)
    public  String priceW(Model model) {
        model.addAttribute("price", saloonDAO.get(1) );
        return "wellnessPrices";
    }

    @RequestMapping(value = "wellness/price", method = RequestMethod.POST)
    public  String priceW(HttpServletRequest request) {
        Saloon saloon = saloonDAO.get(1);
        saloon.setPriceFor1Training(request.getParameter("priceFor1Training"));
        saloon.setTestTraining(request.getParameter("testTraining"));
        saloon.setPriceForFirstMonth(request.getParameter("priceForFirstMonth"));
        saloon.setPriceForSecondMonth(request.getParameter("priceForSecondMonth"));
        saloonDAO.add(saloon);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "wellness/services", method = RequestMethod.GET)
    public  String servicesW(Model model) {
        List<Customer> x = customerDAO.getAll();
        Collections.sort(x, Customer.Comparators.LOGIN);
        model.addAttribute("customers", x);
        return "wellnessServices";
    }

    @RequestMapping(value = "wellness/services", method = RequestMethod.POST)
    public  String servicesW(HttpServletRequest request) {
       String id = request.getParameter("id");
        Customer customer = customerDAO.get(Long.parseLong(id));
//        customer.setBonuses(customer.getBonuses());
//        customer.setName(customer.getName());
//        customer.setSurname(customer.getSurname());
//        customer.setPatronymic(customer.getPatronymic());
//        customer.setCard(customer.getCard());
//        customer.setLogin(customer.getLogin());
//        customer.setPassword(customer.getPassword());
        customer.setWellness(Integer.parseInt(request.getParameter("amount")));
//        customer.setRole(roleDAO.get(2));
//        customer.setToken(customer.getToken());
        customerDAO.add(customer);
        try {
            notification("Wellness", "Количество занятий на вашем счете: "+customer.getWellness(), customer);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "saloon/editing", method = RequestMethod.GET)
    public String editS(Model model){
        model.addAttribute("saloon", saloonDAO.get(1));
        return "saloonEditing";
    }

    @RequestMapping(value = "saloon/editing", method = RequestMethod.POST)
    public String editS(HttpServletRequest request){
        Saloon saloon = saloonDAO.get(1);
//        saloon.setName(request.getParameter("name"));
        saloon.setMobileNumber1(request.getParameter("mobileNumber1"));
        saloon.setMobileNumber2(request.getParameter("mobileNumber2"));
        saloon.setInsta(request.getParameter("insta"));
//        saloon.setWeb1(request.getParameter("web1"));
//        saloon.setWeb2(request.getParameter("web2"));
        saloon.setReg1(request.getParameter("reg1"));
        saloon.setReg2(request.getParameter("reg2"));
//        saloon.setTestTraining(saloon.getTestTraining());
//        saloon.setPriceFor1Training(saloon.getPriceFor1Training());
//        saloon.setPriceForFirstMonth(saloon.getPriceForFirstMonth());
//        saloon.setPriceForSecondMonth(saloon.getPriceForSecondMonth());
        saloonDAO.add(saloon);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "masters", method = RequestMethod.GET)
    public String masters(){
//        Map<Double, Master> rating = new TreeMap<Double, Master>(Collections.reverseOrder());
//        List<Master> masters = masterDAO.getAll();
//        for (Master m : masters){
//            long i = m.getId();
//        List<ServiceHistory> serviceHistories = serviceHistoryDAO.getAllByMasterWithout0(i);
//            double sum = 0;
//            double result = 0;
//            for (ServiceHistory s: serviceHistories){
//                sum = sum + s.getRating();
//                result = sum / serviceHistories.size();
//                result = BigDecimal.valueOf(result).setScale(2,BigDecimal.ROUND_HALF_DOWN).doubleValue();
//            }
//            if (result !=0){
//                rating.put(result, m);
//            }
//        }
//        model.addAttribute("rating", rating);
        return "masters";
    }

    @RequestMapping(value = "masters/adding", method = RequestMethod.GET)
    public String addM(Model model){
        model.addAttribute("positions", positionDAO.getAll());
        return "mastersAdding";
    }

    @RequestMapping(value = "masters/adding", method = RequestMethod.POST)
    public String addM(HttpServletRequest request){
        String position = request.getParameter("position");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        Master master = new Master();
        master.setName(name);
        master.setSurname(surname);
        master.setPosition(positionDAO.get(Integer.parseInt(position)));
        masterDAO.add(master);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "masters/editing", method = RequestMethod.GET)
    public String editM(Model model){
        model.addAttribute("positions", positionDAO.getAll());
        model.addAttribute("masters", masterDAO.getAll());
        return "mastersEditing";
    }

    @RequestMapping(value = "masters/editing", method = RequestMethod.POST)
    public String editM(HttpServletRequest request){
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String position = request.getParameter("position");
        Master master = masterDAO.get(Long.parseLong(id));
        master.setName(name);
        master.setSurname(surname);
        master.setPosition(positionDAO.get(Integer.parseInt(position)));
        masterDAO.add(master);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "masters/deleting/{id}", method = RequestMethod.GET)
    public String deleteM(Model model){
        model.addAttribute("masters", masterDAO.getAll());
        return "mastersDeleting";
    }

    @RequestMapping(value = "masters/deleting/{id}", method = RequestMethod.POST)
    public String deleteM(@PathVariable ("id") int id){
        List<ServiceHistory> serviceHistories = serviceHistoryDAO.getAllByMaster(id);
        for (ServiceHistory s : serviceHistories){
            s.setMaster(masterDAO.get(1));
            serviceHistoryDAO.add(s);
        }
        masterDAO.delete(id);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "clients", method = RequestMethod.GET)
    public String clients(){ return "clients";}

    @RequestMapping(value = "prices", method = RequestMethod.GET)
    public String prices(){ return "prices";}

    @RequestMapping(value = "prices/adding", method = RequestMethod.GET)
    public String addP(){ return "pricesAdding";}

    @RequestMapping(value = "prices/adding", method = RequestMethod.POST)
    public String addP(HttpServletRequest request){
        String name = request.getParameter("name");
        String price = request.getParameter("price");
        String param = request.getParameter("param");
        Services services = new Services();
        services.setName(name);
        services.setPrice(Integer.valueOf(price));
        services.setParam(Integer.parseInt(param));
        servicesDAO.add(services);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "prices/editing", method = RequestMethod.GET)
    public String editP(Model model){
        model.addAttribute("services", servicesDAO.getAllM());
        model.addAttribute("brovki", servicesDAO.getAllB());
        model.addAttribute("resni4ki", servicesDAO.getAllR());
        return "pricesEditing";
    }

    @RequestMapping(value = "prices/editing", method = RequestMethod.POST)
    public String editP(HttpServletRequest request){
        String name = request.getParameter("name");
        String price = request.getParameter("price");
        String newName = request.getParameter("newName");
        String yn = request.getParameter("yn");
        Services services = servicesDAO.get(Long.parseLong(name));
        if(yn.equalsIgnoreCase("0")) services.setName(services.getName());
        else services.setName(newName);
        if (price.equalsIgnoreCase("")) services.setPrice(services.getPrice());
        else services.setPrice(Integer.valueOf(price));
        servicesDAO.add(services);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "prices/deleting/{id}", method = RequestMethod.GET)
    public String deleteP(Model model){
        model.addAttribute("services", servicesDAO.getAllM());
        return "pricesDeleting";
    }

    @RequestMapping(value = "prices/deleting/{id}", method = RequestMethod.POST)
    public String deleteP(@PathVariable("id") int id){
        Services services = servicesDAO.get(id);
        services.setParam(-1);
        servicesDAO.add(services);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "prices/info", method = RequestMethod.GET)
    public String infoP(Model model){
        model.addAttribute("services", servicesDAO.getAllM());
        model.addAttribute("brovki", servicesDAO.getAllB());
        model.addAttribute("resni4ki", servicesDAO.getAllR());
        return "pricesInfo";
    }

    @RequestMapping(value = "offers", method = RequestMethod.GET)
    public String offers(){ return "offers";}

    @RequestMapping(value = "offers/adding", method = RequestMethod.GET)
    public String addO(Model model){
        List<Customer> x = customerDAO.getAll();
        Collections.sort(x, Customer.Comparators.LOGIN);
        model.addAttribute("customers", x);
        return "offersAdding";
    }

    @RequestMapping(value = "offers/adding", method = RequestMethod.POST)
    public String addO(HttpServletRequest request) throws ParseException {
        String[] customer = request.getParameterValues("customerer");
        for (String c:customer) {
            String name = request.getParameter("name");
            String text = request.getParameter("taxt");
            Advertising advertising = new Advertising();
            advertising.setName(name);
            advertising.setText(text);
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(request.getParameter("data1"));
            Date date2 = sdf.parse(request.getParameter("data2"));
            advertising.setDate1(date1);
            advertising.setDate2(date2);
            advertising.setCustomer(customerDAO.get(Long.parseLong(c)));
            try {
                notification("Magic Nails. Акция.", name,customerDAO.get(Long.parseLong(c)));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            advertisingDAO.add(advertising);
        }
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "offers/editing", method = RequestMethod.GET)
    public String editO(Model model){
        model.addAttribute("offers", advertisingDAO.getAll());
        return "offersEditing";}

    @RequestMapping(value = "offers/editing", method = RequestMethod.POST)
    public String editO(HttpServletRequest request) throws ParseException {
        String name = request.getParameter("name");
        List<Advertising>  advertisings = advertisingDAO.getAdvertisingByName(name);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        Date date1 = sdf.parse(request.getParameter("data1"));
        Date date2 = sdf.parse(request.getParameter("data2"));
        String newName = request.getParameter("newName");
        String newText = request.getParameter("newText");
        for (Advertising a : advertisings ){
            a.setCustomer(a.getCustomer());
            a.setDate1(date1);
            a.setDate2(date2);
            a.setName(newName);
            a.setText(newText);
            try {
                notification("Персональное предложение:", a.getName(), a.getCustomer());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            advertisingDAO.add(a);
        }
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "offers/info", method = RequestMethod.GET)
    public String infoO(Model model){
        model.addAttribute("offers", advertisingDAO.getAll());
        return "offersInfo";}

    @RequestMapping(value = "offers/deleting", method = RequestMethod.GET)
    public String deleteO(Model model){
        model.addAttribute("offers", advertisingDAO.getAll());
        return "offersDeleting";}

    @RequestMapping(value = "offers/deleting/{name}", method = RequestMethod.POST)
    public String deleteO(@PathVariable ("name") String name){
        List<Advertising>  advertisings = advertisingDAO.getAdvertisingByName(name);
        for (Advertising a : advertisings){
            advertisingDAO.delete(a.getId());
        }
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "services", method = RequestMethod.GET)
    public String services(){ return "services";}

    @RequestMapping(value = "services/adding", method = RequestMethod.GET)
    public String addS(Model model){

        List<Customer> x = customerDAO.getAll();
        Collections.sort(x, Customer.Comparators.LOGIN);
        model.addAttribute("customers", x);
        model.addAttribute("services", servicesDAO.getAllM());
        model.addAttribute("brovki", servicesDAO.getAllB());
        model.addAttribute("resni4ki", servicesDAO.getAllR());
        model.addAttribute("masters", masterDAO.getAll());
        return "servicesAdding";
    }

    @RequestMapping(value = "services/adding", method = RequestMethod.POST)
    public String addS(ServiceHistory serviceHistory, HttpServletRequest request) throws ParseException, UnsupportedEncodingException {
        String choise = request.getParameter("bonuces");
        String[] services = request.getParameterValues("serviceer");
        int proc = Integer.parseInt(request.getParameter("proc"));
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(request.getParameter("data"));
        String user = request.getParameter("customerer");
        double nailPrice = Double.parseDouble(request.getParameter("nailsPrice"));
        Customer customer = customerDAO.get(Long.parseLong(user));
        double b = 0;
        b = Double.parseDouble(request.getParameter("b"));
        double bonuces;
        if(choise.equalsIgnoreCase("0"))bonuces = 0;  //нет
        if(choise.equalsIgnoreCase("1"))bonuces = customer.getBonuses(); //да
        else bonuces = b;
        double price = 0;
        for(String s: services){
            Services services1 = servicesDAO.get((Integer.parseInt(s)));
            price = price + services1.getPrice();
        }
        price = price + nailPrice;
        price = (price/100)*(100-proc);
        if(price>=bonuces){
            customer.setBonuses((new BigDecimal((price-bonuces)*0.02+customer.getBonuses()-bonuces).setScale(2, RoundingMode.UP).doubleValue()));  //ОШИБКА!!
        }
        else if(price<bonuces){
            customer.setBonuses(bonuces-price);
        }
//        customer.setName(customer.getName());
//        customer.setSurname(customer.getSurname());
//        customer.setPatronymic(customer.getPatronymic());
//        customer.setCard(customer.getCard());
//        customer.setLogin(customer.getLogin());
//        customer.setPassword(customer.getPassword());
//        customer.setWellness(customer.getWellness());
//        customer.setToken(customer.getToken());
//        customer.setRole(roleDAO.get(2));
        customerDAO.add(customer);
        Master master = masterDAO.get(Integer.parseInt(request.getParameter("masterer")));
        for(String s: services){
            serviceHistory = new ServiceHistory();
            serviceHistory.setDate(date);
            serviceHistory.setCustomer(customer);
            Services services1 = servicesDAO.get(Long.parseLong(s));
            serviceHistory.setService(services1);
            serviceHistory.setMaster(master);
            if(services1.getPrice()>bonuces){
                serviceHistory.setBonuces(bonuces+(services1.getPrice()/100*proc));
                bonuces = 0;
            }
            else if(services1.getPrice()<=bonuces){
                serviceHistory.setBonuces(services1.getPrice());
                bonuces = bonuces - services1.getPrice()+(services1.getPrice()/100*proc);
            }

            String t = "Magic Nails";
            String x = customer.getBonuses()+" - остаток бонусов на вашем счете. Пожалуйста оцените работу мастера, мы работаем над нашим сервисом:)";
            notification(t,x,customer);
            serviceHistoryDAO.add(serviceHistory);
        }

        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "services/deleting", method = RequestMethod.GET)
    public String deleteS(Model model){
        List<ServiceHistory> s = serviceHistoryDAO.getAll();
        Collections.reverse(s);
        model.addAttribute("historys", s);
        return "servicesDeleting";
    }

    @RequestMapping(value = "services/deleting", method = RequestMethod.POST)
    public String deleteS(HttpServletRequest request){
        serviceHistoryDAO.delete(Long.parseLong(request.getParameter("service")));
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "services/info/{day}", method = RequestMethod.GET)
    public String infoSAnotherDay(Model model, @PathVariable("day") String day){
        List<ServiceHistory> s = serviceHistoryDAO.getAllByDate(day);
        String year = day.substring(0,4);
        String month = day.substring(5,7);
        String d = day.substring(8,10);
        String cal = year+"-"+month+"-"+d;
        String str = d+"."+month+"."+year;
        model.addAttribute("cal", cal);
        model.addAttribute("str", str);
        Collections.reverse(s);
        model.addAttribute("history", s);
        return "servicesInfoDay";
    }

    @RequestMapping(value = "other", method = RequestMethod.GET)
    public String other(){ return "other";}

    @RequestMapping(value = "clients/adding", method = RequestMethod.GET)
    public String addC(){ return "clientsAdding";}

    @RequestMapping(value = "clients/adding", method = RequestMethod.POST)
    public String addC(Customer customer, HttpServletRequest request, BindingResult result)  {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String name2 = request.getParameter("name2");
        String card = request.getParameter("card");
        String passwod = request.getParameter("phone");
        String bonuces = request.getParameter("bonuces");
        String wellness = request.getParameter("wellness");
        String phone = request.getParameter("phone");
        customer.setName(name);
        customer.setDate(request.getParameter("bDate"));
        customer.setSurname(surname);
        customer.setPatronymic(name2);
        customer.setCard(card);
        customer.setLogin(card);
        customer.setPhone(phone);
        customer.setPassword(passwod);
        customer.setBonuses(Double.parseDouble(bonuces));
        customer.setWellness(Integer.parseInt(wellness));
        customer.setRole( roleDAO.get(2));
        customer.setToken(customer.getToken());
        if(result.hasErrors()){
            return "redirect:/magicnail/main";
        }
        customerDAO.add(customer);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "clients/editing", method = RequestMethod.GET)
    public String editC(Model model){
        List<Customer> x = customerDAO.getAll();
        Collections.sort(x, Customer.Comparators.LOGIN);
        model.addAttribute("customers", x);
        return "clientsEditing";
    }

    @RequestMapping(value = "clients/editing", method = RequestMethod.POST)
    public String editC(HttpServletRequest request){
        String id = request.getParameter("id");
        return "redirect:/magicnail/clients/cab/"+id;
    }

    @RequestMapping(value = "clients/cab/{id}", method = RequestMethod.GET)
    public String cabC(Model model, @PathVariable("id") int id ){
        model.addAttribute("customer", customerDAO.get(id));
        return "clientsCab";
    }

    @RequestMapping(value = "clients/cab/{id}", method = RequestMethod.POST)
    public String cabC(HttpServletRequest request, @PathVariable("id") int id){
        Customer customer = customerDAO.get(id);
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String name2 = request.getParameter("name2");
        String card = request.getParameter("card");
        String login = request.getParameter("login");
        String passwod = request.getParameter("password");
        String phone = request.getParameter("phone");
        String bonuces = request.getParameter("bonuces");
        String wellness = request.getParameter("wellness");
        String bDate = request.getParameter("bDate");
        customer.setName(name);
        customer.setSurname(surname);
        customer.setDate(bDate);
        customer.setPatronymic(name2);
        customer.setCard(card);
        customer.setLogin(login);
        customer.setPassword(passwod);
        customer.setPhone(phone);
        customer.setBonuses(Double.parseDouble(bonuces));
        customer.setWellness(Integer.parseInt(wellness));
//        customer.setRole( roleDAO.get(2));
//        customer.setToken(customer.getToken());
        customerDAO.add(customer);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "clients/deleting/{id}", method = RequestMethod.GET)
    public String deleteC(Model model){
        model.addAttribute("customers", customerDAO.getAll());
        return "clientsDeleting";
    }

    @RequestMapping(value = "clients/deleting/{id}", method = RequestMethod.POST)
    public String deleteC(@PathVariable("id") int id) {
        customerDAO.delete(id);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "clients/info", method = RequestMethod.GET)
    public String infoC(Model model){
        List<Customer> x = customerDAO.getAll();
        Collections.sort(x, Customer.Comparators.LOGIN);
        model.addAttribute("customers", x);
        return "clientsInfo";
    }

    @RequestMapping(value = "/imageDisplay/{id}", method = RequestMethod.GET)
    public void showImage(HttpServletResponse response, HttpServletRequest request, @PathVariable ("id") int id)
            throws ServletException, IOException {
        Customer customer = customerDAO.get(id);
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        byte[] encodeBase64 = Base64.getDecoder().decode(customer.getImg());
        response.getOutputStream().write(encodeBase64);
        response.getOutputStream().close();
    }

    @RequestMapping(value = "image/{id}", method = RequestMethod.GET)
    public String img(Model model, @PathVariable("id") int id){
        model.addAttribute("id", id);
        return "image";
    }

    @RequestMapping(value = "dbRemoving/{day}", method = RequestMethod.GET)
    public String dbDelete(@PathVariable("day")String day) throws ParseException {
        //2017-02-10
        String year = day.substring(0,4);
        String other = day.substring(4,10);
        Saloon saloon = saloonDAO.get(1);

        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(day);
        saloon.setDbRemove(date);
        saloonDAO.add(saloon);
        int y = Integer.parseInt(year);
        y = y -1;
        String res = y + other;
        System.out.println(res);
        List<ServiceHistory> serviceHistories = serviceHistoryDAO.getOld(res);
        for (ServiceHistory s : serviceHistories){
           serviceHistoryDAO.delete(s.getId());
        }
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "positions", method = RequestMethod.GET)
    public String positions(){
        return "positions";
    }


    @RequestMapping(value = "positions/adding", method = RequestMethod.GET)
    public String positionAdd(Model model){
        model.addAttribute("adress", adressDAO.getAll());
        return "positionsAdding";
    }

    @RequestMapping(value = "positions/adding", method = RequestMethod.POST)
    public String positionAdd(HttpServletRequest request){
        String name = request.getParameter("name");
        String adress = request.getParameter("adress");
        Position position = new Position();
        position.setAdress(adressDAO.get(Integer.parseInt(adress)));
        position.setName(name);
        positionDAO.add(position);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "positions/editing", method = RequestMethod.GET)
    public String positionEdit(Model model){
        model.addAttribute("positions", positionDAO.getAll());
        model.addAttribute("adress", adressDAO.getAll());
        return "positionEditing";
    }

    @RequestMapping(value = "positions/editing", method = RequestMethod.POST)
    public String positionEdit(HttpServletRequest request){
        Position position = positionDAO.get(Integer.parseInt(request.getParameter("position")));
        position.setName(request.getParameter("name"));
        positionDAO.add(position);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "positions/deleting/{id}", method = RequestMethod.POST)
    public String deletePos(@PathVariable("id") int id){
        List<Master> masters = masterDAO.getAllByPos(id);
        for (Master m : masters){
            m.setPosition(positionDAO.get(5));
            masterDAO.add(m);
        }
        positionDAO.delete(id);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "adress/adding", method = RequestMethod.GET)
    public String adressAdd(){
        return "adressAdding";
    }

    @RequestMapping(value = "adress/adding", method = RequestMethod.POST)
    public String adressAdd(HttpServletRequest request){
       String name = request.getParameter("adress");
        Adress adress = new Adress();
        adress.setName(name);
        adressDAO.add(adress);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "adress/editing", method = RequestMethod.GET)
    public String adressEdit(Model model){
        model.addAttribute("adress", adressDAO.getAll());
        return "adressEditing";
    }

    @RequestMapping(value = "adress/editing", method = RequestMethod.POST)
    public String adressEdit(HttpServletRequest request){
        Adress adress = adressDAO.get(Integer.parseInt(request.getParameter("adress")));
        adress.setName(request.getParameter("name"));
        adressDAO.add(adress);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "adress/deleting/{id}", method = RequestMethod.POST)
    public String deleteA(@PathVariable("id") int id){
        List<Position> positions = positionDAO.getAllById(id);
        for (Position p : positions){
            p.setAdress(adressDAO.get(2));
            positionDAO.add(p);
        }
        adressDAO.delete(id);
        return "redirect:/magicnail/main";
    }

    @RequestMapping(value = "appointment/adding", method = RequestMethod.GET)
    public String addAppointment(Model model){
        model.addAttribute("customers", customerDAO.getAll());
        return "appointmentAdding";
    }

    public void notification(String z, String b, Customer customer) throws UnsupportedEncodingException {
        JSONObject body = new JSONObject();
        body.put("to", customer.getToken());
        body.put("sound", "default");
        body.put("content_available", true);
        JSONObject notification = new JSONObject();
        byte[] title = z.getBytes("UTF-8");
        String encodedTitle = Base64.getEncoder().encodeToString(title);
        byte[] text = b.getBytes("UTF-8");
        String encodedText = Base64.getEncoder().encodeToString(text);
        notification.put("body", encodedText);
        notification.put("title", encodedTitle);
        JSONObject data = new JSONObject();
        body.put("data", notification);
        HttpEntity<String> request1 = new HttpEntity<String>(body.toString());
        CompletableFuture<FirebaseResponse> pushNotification = androidPushNotificationsService.send(request1);
        CompletableFuture.allOf(pushNotification).join();
    }

    @RequestMapping(value = "appointment/adding", method = RequestMethod.POST)
    public String addAppointment(HttpServletRequest request) throws ParseException, UnsupportedEncodingException {
        String client = request.getParameter("name");
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date date = sdf.parse(request.getParameter("data"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Appointment appointment = new Appointment();
        Customer customer = customerDAO.get(Long.parseLong(client));
        appointment.setCustomer(customer);
        appointment.setTime(date);
        appointmentDAO.add(appointment);
        String v = "Запись";
        String t = String.format("Ваш визит %s.%s на %s:%s в студию Magic Nails 0972702700; ул. Чавдар 2, оф. 197", calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
        notification(v,t,customer);
        return "redirect:/magicnail/main";
    }


    @RequestMapping(value = "rating", method = RequestMethod.GET)
    public String rating(Model model){
        Map<Double, Master> rating = new TreeMap<Double, Master>(Collections.reverseOrder());
        List<Master> masters = masterDAO.getAll();
        for (Master m : masters){
            long i = m.getId();
            List<ServiceHistory> serviceHistories = serviceHistoryDAO.getAllByMasterWithout0(i);
            double sum = 0;
            double result = 0;
            for (ServiceHistory s: serviceHistories){
                sum = sum + s.getRating();
                result = sum / serviceHistories.size();
                result = BigDecimal.valueOf(result).setScale(2,BigDecimal.ROUND_HALF_DOWN).doubleValue();
            }
            if (result !=0){
                rating.put(result, m);
            }
        }
        model.addAttribute("rating", rating);
        return "rating";
    }
}




