package com.project.magicMobileApp.controllers;

import com.project.magicMobileApp.controllers.firebase.AndroidPushNotificationsService;
import com.project.magicMobileApp.crud.*;
import com.project.magicMobileApp.models.*;
import com.project.magicMobileApp.models.ModelsForApp.*;
import com.project.magicMobileApp.models.ModelsForApp.MainModels.CustomerModel;
import com.project.magicMobileApp.models.ModelsForApp.MainModels.SaloonModel;
import com.project.magicMobileApp.models.ModelsForApp.PriceTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServlet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping("/magic/")
public class RestController extends HttpServlet {

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private ServiceHistoryDAO serviceHistoryDAO;
    @Autowired
    private SaloonDAO saloonDAO;
    @Autowired
    private ServicesDAO servicesDAO;
    @Autowired
    private AdvertisingDAO advertisingDAO;
    @Autowired
    AndroidPushNotificationsService androidPushNotificationsService;
    @Autowired
    private RoleDAO roleDAO;

    @RequestMapping(value = "/customerJSON/{login}/{password}/{token}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    CustomerModel returnCustomer(@PathVariable("login") String login, @PathVariable("password") String password, @PathVariable("token") String token){
        try {
            Customer customer = customerDAO.getByLogin(login);
            Date lastUse = new Date();
            customer.setLastUse(lastUse);
            if (customer.getPassword().equalsIgnoreCase(password)) {
                customer.setToken(token);
                String name = customer.getName();
                String surname = customer.getSurname();
                String patronymic = customer.getPatronymic();
                double bonuces = customer.getBonuses();
                String card = customer.getCard();
                int wellness = customer.getWellness();
                customer.setName(name);
                customer.setSurname(surname);
                customer.setPatronymic(patronymic);
                customer.setBonuses(bonuces);
                customer.setCard(card);
                customer.setWellness(wellness);
                customer.setRole(roleDAO.get(2));
                customer.setPassword(customer.getPassword());
                customer.setLogin(customer.getLogin());
                CustomerModel customerModel = new CustomerModel();
                customerModel.setId(String.valueOf(customer.getId()));
                customerModel.setNameCustomer(name);
                customerModel.setSurname(surname);
                customerModel.setSecondName(patronymic);
                customerModel.setBonuces(String.valueOf(customer.getBonuses()));
                customerModel.setCard(String.valueOf(card));
                customerModel.setWellnessAmount(String.valueOf(wellness));
                List<ServiceHistory> serviceHistory = serviceHistoryDAO.getById(customer.getId());
                ArrayList<ServiceModel> serviceModels = new ArrayList<ServiceModel>();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                for (ServiceHistory sh : serviceHistory) {
                    serviceModels.add(new ServiceModel(
                            simpleDateFormat.format(sh.getDate()).toString(),
                            sh.getService().getName(),
                            sh.getMaster().getName(),
                            String.valueOf(sh.getService().getPrice() - sh.getBonuces()),
                            String.valueOf(sh.getId()),
                            String.valueOf(sh.getRating()),
                            sh.getService().getParam()));
                }
                Collections.reverse(serviceModels);
                customerModel.setServiceList(serviceModels);
                List<Advertising> advertisings = advertisingDAO.getAllbyId((int) customer.getId());
                List<AdvertisingModel> x = new ArrayList<AdvertisingModel>();
                for (Advertising a : advertisings) {
                    x.add(new AdvertisingModel(
                            a.getName(),
                            a.getText(),
                            simpleDateFormat.format(a.getDate1()).toString(),
                            simpleDateFormat.format(a.getDate2()).toString()));
                }
                customerModel.setAdvertisingModels(x);

                String birthday = customer.getDate();
                System.out.println(birthday);
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd.MM");
                Date date = null;
                try {
                    date = sdf.parse(birthday);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println(date);
                customerModel.setBirthday(date);
                customerDAO.add(customer);
                return customerModel;
            }
        } catch (NoResultException e) {
            return null;
        }
        return null;
    }


    @RequestMapping(value = "/saloonJSON", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    SaloonModel returnSaloon() {
        Saloon saloon = saloonDAO.get(1);
        List<Services> s = servicesDAO.getAllM();
        List<PriceModel> priceModels = new ArrayList<PriceModel>();
        for (Services S : s) {
            priceModels.add(new PriceModel(S.getName(), String.valueOf(S.getPrice())));
        }
        s = servicesDAO.getAllB();
        List<PriceModel> priceModels2 = new ArrayList<PriceModel>();
        for (Services S : s) {
            priceModels2.add(new PriceModel(S.getName(), String.valueOf(S.getPrice())));
        }
        s = servicesDAO.getAllR();
        List<PriceModel> priceModels3 = new ArrayList<PriceModel>();
        for (Services S : s) {
            priceModels3.add(new PriceModel(S.getName(), String.valueOf(S.getPrice())));
        }
        List<Services> wellness = servicesDAO.getAllW();
        List<String> wellnessNames = new ArrayList<String>();
        for (Services x : wellness) wellnessNames.add(x.getName());
        WellnessModel wellnessModel = new WellnessModel();
        wellnessModel.setNames(wellnessNames);
        List<String> prices = new ArrayList<String>();
        prices.add(saloon.getTestTraining());
        prices.add(saloon.getPriceFor1Training());
        prices.add(saloon.getPriceForFirstMonth());
        prices.add(saloon.getPriceForSecondMonth());
        wellnessModel.setPrices(prices);
        List<PriceTypeModel> res = new ArrayList<PriceTypeModel>();
        PriceTypeModel x = new PriceTypeModel("Маникюр", priceModels);
        PriceTypeModel y = new PriceTypeModel("Бровки", priceModels2);
        PriceTypeModel z = new PriceTypeModel("Реснички", priceModels3);
        res.add(x);
        res.add(y);
        res.add(z);
        SaloonModel saloonModel1 = new SaloonModel(wellnessModel, res, saloon.getMobileNumber1(), saloon.getMobileNumber2(), saloon.getInsta(), saloon.getReg2(),  saloon.getReg1());
        return saloonModel1;
    }

    @RequestMapping(value = "/customerJSON/serviceRating/{id}/{rating}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    ServiceModel Stars(@PathVariable("id") int id, @PathVariable("rating") int rating){
        ServiceHistory serviceHistory = serviceHistoryDAO.get(id);
        serviceHistory.setBonuces(serviceHistory.getBonuces());
        serviceHistory.setCustomer(serviceHistory.getCustomer());
        serviceHistory.setMaster(serviceHistory.getMaster());
        serviceHistory.setService(serviceHistory.getService());
        serviceHistory.setDate(serviceHistory.getDate());
        serviceHistory.setRating(rating);
        serviceHistoryDAO.add(serviceHistory);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        ServiceModel serviceModel = new ServiceModel(
                simpleDateFormat.format(serviceHistory.getDate()).toString(),
                serviceHistory.getService().getName(),
                serviceHistory.getMaster().getName(),
                String.valueOf(serviceHistory.getService().getPrice() - serviceHistory.getBonuces()),
                String.valueOf(serviceHistory.getId()),
                String.valueOf(serviceHistory.getRating()),
                serviceHistory.getService().getParam());
        return serviceModel;
    }

    @RequestMapping(value = "/customerJSON/{id}/image", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> saveImg(@PathVariable ("id") int id, @RequestBody  String files){
        Customer customer = customerDAO.get(id);
        customer.setImg(files);
        customerDAO.add(customer);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    @RequestMapping(value = "/customerJSON/{id}/imageget", method = RequestMethod.GET)
    @ResponseBody
    public String Img(@PathVariable ("id") int id){
        Customer customer = customerDAO.get(id);
        String res = customer.getImg();
        return res;
    }

}

