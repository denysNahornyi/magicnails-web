package com.project.magicMobileApp.controllers;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServlet;

/**
 * Created by denys on 16.01.17.
 */
@Controller
@RequestMapping("main/")
public class MainPageController {
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public  String authorization() {return "index";}
}
