<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Главная страница</title>

    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">

    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="MN">MagicNails</div>
            <div id="text">Работа с акциями</div>
            <div class="col-md-6 .col-md-offset-3">
                <ul class="social">
                    <li class="clients">  <a href="offers/adding"><div class="x">Новая</div><div class="n">акция</div></a></li>
                    <li class="offers"><a href="offers/editing">Редакция</a></li>
                    <li class="fifth"><a href="offers/info"><div class="x">Все</div><div class="n">акции</div></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <div id="left">
        <a href="main">Главная</a> | <a href="clients">Клиенты</a> |  <a href="services">Услуги</a> | <a href="offers">Рассылка</a> | <a href="prices">Цены</a> | <a href="wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>

</body>
</html>
