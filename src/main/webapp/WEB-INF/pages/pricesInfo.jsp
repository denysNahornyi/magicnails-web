<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
    <style>
        table{
            margin-top: 1%;
            margin-bottom: 5%;
            padding: 10px;
            border-collapse: collapse;

            /*font-size: 100%;*/
        }
        td {
            height: 40px;
            vertical-align: bottom;
            border: solid black 1px;
        }


        th{
            background: #ED9FC1;
            border: solid black 1px;
        }




    </style>
</head>
<body>
<table  align="center" cellpadding="5" ><!---->
        <tr>
            <th colspan="2" align="center" >
                Прайс лист салона:
            </th>
        </tr>

        <tr>
            <th colspan="2" align="center">
                Цены на маникюр:
            </th>
        </tr>
        <tr>
            <th align="center" >
                Название услуги
            </th>
            <th align="center">
               Цена
            </th>
        </tr>
        <c:forEach items="${services}" var="service">
            <tr>
                <td align="center">
                        ${service.name}
                </td>
                <td align="center">
                        ${service.price}грн.
                </td>
            </tr>
        </c:forEach>


        <tr>
             <th colspan="2" align="center" >
                Цены на бровки:
             </th>
        </tr>
            <tr>
                <th align="center" >
                    Название услуги
                </th>
                <th align="center" >
                    Цена
                </th>
            </tr>
            <c:forEach items="${brovki}" var="brov">
                <tr>
                    <td align="center">
                            ${brov.name}
                    </td>
                    <td align="center">
                            ${brov.price}грн.
                    </td>
                </tr>
            </c:forEach>
    <tr>
        <th colspan="2" align="center">
            Цены на реснички:
        </th>
    </tr>
    <tr>
        <th align="center">
            Название услуги
        </th>
        <th align="center">
            Цена
        </th>
    </tr>
    <c:forEach items="${resni4ki}" var="res">
        <tr>
            <td align="center">
                    ${res.name}
            </td>
            <td align="center">
                    ${res.price}грн.
            </td>
        </tr>
    </c:forEach>

    </table>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
