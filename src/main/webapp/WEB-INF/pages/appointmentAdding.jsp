<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Запись клиента</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 6%;
            font-size: 100%;
        }
        td {
            height: 40px;
            vertical-align: center;
            padding-bottom: 1.8%;
        }
    </style>
</head>
<body onload="myFunction()">
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
    function CheckName(val) {
        var element=document.getElementById('b')
        if(val=='2')
            element.style.display='block'
        else
            element.style.display = 'none'
    }
    function myFunction() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        }

        if(mm<10) {
            mm='0'+mm
        }

        today = yyyy+'-'+mm+'-'+dd+"T12:00:00.000";
        document.getElementById("data").value = today;
    }
</script>
<form:form action="adding" method="post" acceptCharset="UTF-8" onsubmit="zapret()">
    <table cellspacing="1" align="center" >
        <tr>
            <td colspan="2">
                <center><h3>Запись клиента:</h3></center>
            </td>
        </tr>
        <tr>
            <td >
                Время записи:
            </td>
            <td>
                <input type="datetime-local" id="data" name="data">
            </td>
        </tr>
        <tr>
            <td>
                Имя клиента:
            </td>
            <td>
               <center> <select id="name"  name="name" required>
                    <c:forEach items="${customers}" var="customer">
                        <option value="${customer.id}">${customer.card} | ${customer.surname} ${customer.name}</option>
                    </c:forEach>
                </select></center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><input type="submit" id="s" value="Все верно"></center>
            </td>
        </tr>
    </table>
</form:form>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp//magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp//magicnail/services">Услуги</a> | <a href="/Mobileapp//magicnail/offers">Рассылка</a> | <a href="/Mobileapp//magicnail/prices">Цены</a> | <a href="/Mobileapp//magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp//magicnail/other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp//magicnail/exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>



</body>
</html>
