<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Удаление услуги</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 6%;
            font-size: 100%;
        }
        td {
            height: 40px;
            vertical-align: bottom;
        }
    </style>
</head>
<body>
<div id="main">
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>
<form:form action="deleting" method="post" acceptCharset="UTF-8" onsubmit="zapret()">
<table cellspacing="1" align="center" >
    <tr>
        <td colspan="2">
            <center><h3>Удаление сервиса</h3></center>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top">
            Выбор услуги
        </td>
        <td  >
            <select id="service" name="service" onmousedown="if(this.options.length>10){this.size=10;}"  onchange='this.size=0;' onblur="this.size=0;" required>
                <c:forEach items="${historys}" var="history">
                    <option value="${history.id}">${history.date.date}.${history.date.month+1}.${history.date.year+1900} | ${history.customer.surname} ${history.customer.name} - ${history.service.name}</option>
                </c:forEach>
            </select>
        </td>
    </tr>




    <tr>
        <td width="100px"  colspan="2" >
            <center><input type="submit" id="s" value="Удалить"></center>
        </td>
    </tr>
        </form:form>


</table>
</div>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp//magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp//magicnail/services">Услуги</a> | <a href="/Mobileapp//magicnail/offers">Рассылка</a> | <a href="/Mobileapp//magicnail/prices">Цены</a> | <a href="/Mobileapp//magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp//magicnail/other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp//magicnail/exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>





