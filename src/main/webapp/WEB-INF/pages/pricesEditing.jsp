<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
    <style>
        table{
            margin-top: 6%;
            margin-bottom: 5%;
            padding: 10px;
            font-size: 100%;

        }
        td {
            height: 40px;
            vertical-align: bottom;
            padding-bottom: 1.8%;
            height: 40px;
        }

        #price{
            width: 14%;
        }
    </style>
</head>
<body>
<script>
    function  getRequest() {
        var form = document.getElementById("user")|| null
        if(form){
            var sel = document.getElementById('name');
            var index = sel.options[sel.selectedIndex].value;
            form.action = "deleting/"+index
            form.method= "post"
            form.submit()
        }
    }
    function CheckName(val) {
        var element=document.getElementById('x')
        if(val=='1')
            element.style.display='block'

        else
            element.style.display = 'none'
    }
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
        var del = document.getElementById('d').disabled = true;
    }
</script>
<div id="main">
<form:form action="editing" method="post" acceptCharset="UTF-8" onsubmit="zapret()">



<table cellspacing="1" align="center" >
    <tr>
        <td colspan="2">
            <center><h3>Редактирование услуги</h3></center>
        </td>
    </tr>
    <tr>
        <td>
            Выбор услуги
        </td>
        <td>
            <select id="name" name="name">
                <optgroup label="Маникюр">
                    <c:forEach items="${services}" var="service">
                        <option value="${service.id}">${service.name}  ${service.price}грн.</option>
                    </c:forEach>
                </optgroup>
                <optgroup label="Бровки">
                    <c:forEach items="${brovki}" var="brov">
                        <option value="${brov.id}">${brov.name}  ${brov.price}грн.</option>
                     </c:forEach>
                </optgroup>
                <optgroup label=" Реснички">
                    <c:forEach items="${resni4ki}" var="res">
                        <option value="${res.id}">${res.name}  ${res.price}грн.</option>
                    </c:forEach>
                </optgroup>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            Новая цена
        </td>
        <td align="center">
           <input type="text"  id="price" name="price" required pattern="^([1-9][0-9]{0,2}|1000)$">
        </td>
    </tr>
    <tr>
        <td>
            Желаете ли изменить имя?
        </td>
        <td>
           <center> <select id="yn" name="yn"  onchange='CheckName(this.value);' required>
                <option value="0">Нет.</option>
                <option value="1">Да.</option>
            </select></center>
        </td>
    </tr>
    <tr>
        <td colspan = "2"  >
           <center><input type="text" align="center" style="display: none ;" placeholder="Новое имя" id="x" name="newName"  pattern="^[А-ЯЁ][а-яё\s\-]+$" title="Название услуги русским языком. Первая буква заглавная." maxlength="30"></center>
        </td>
    </tr>
    <tr>
        <td  align="center">
           <input type="submit" id="s" value="Редактировать">
        </td>

        </form:form>
        <form:form id="user" acceptCharset=" UTF-8" >
            <td  align="center">
                <input onclick="getRequest(); zapret()" id="d" type="submit" value="Удалить">
            </td>
        </form:form>
    </tr>
</table>
</div>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img"  src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img"  src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>





