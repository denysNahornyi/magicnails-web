<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Новый клиент</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 0%;
            font-size: 100%;
        }
        td {
            padding-bottom: 1.8%;
            height: 40px;
            vertical-align: bottom;
        }

    </style>
</head>
<body>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>
<div id="main">
<form:form action="adding" method="post" acceptCharset=" UTF-8" onsubmit="zapret()" >
    <table cellspacing="1" align="center" >
       <tr>
           <td colspan="2">
               <center><h3>Добавление клиента:</h3></center>
           </td>
       </tr>
        <tr>
            <td>
                Фамилия:
            </td>
                <td>
                <input type="text"  id="surname" name="surname" required pattern="^[А-ЯЁ][а-яё]+$" title="Фамилия пользователя русским языком. Первая буква заглавная." maxlength="20" >
            </td>
        </tr>
        <tr>
                <td>
                Имя:
            </td>
                <td>
                <input type="text" id="name" name="name" required pattern="^[А-ЯЁ][а-яё]+$" title="Имя пользователя русским языком. Первая буква заглавная." maxlength="20">
            </td>
        </tr>
        <tr>
                <td>
                Отчество:
            </td>
                <td>
                <input type="text" id="name2" name="name2" required pattern="^[А-ЯЁ-][а-яё]+$|^[-]" title="Фамилия пользователя русским языком. Первая буква заглавная. Либо -" maxlength="20">
            </td>
        </tr>
        <tr>
                <td>
                Дата рождения:
            </td>
                <td>
                <input type="text" id="bDate" name="bDate" placeholder="дд.мм" required pattern="(^[0-3][0-9]\.(1[0-2]|0?[1-9]))" title="Данные введены неверным образом. Пример: 09.03." maxlength="5" minlength="5" >
            </td>
        </tr>
        <tr>
                <td>
                Номер карты:
            </td>
                <td>
                <input type="text" id="card" name="card"  required pattern="[0-9]{4,4}" title="Введите 4 цыфры карты." maxlength="4" minlength="4">
            </td>
        </tr>
        <tr>
                <td>
                Номер мобильного:
            </td>
            <td>
                <input type="text" id="phone" name="phone" required pattern="(0[0-9]{9,9})" title="Номер без кода страны и без разделителей. Пример: 0975436753" maxlength="10" minlength="10">
            </td>
        </tr>
        <tr>
                <td>
                К-во бонусов:
            </td>
                <td>
                <input type="text" id="bonuces" name="bonuces" value="0" required pattern="^[0-9]{1,3}[.]{0,1}[0-9]{0,2}" maxlength="6" minlength="1" onkeypress='return event.charCode >=48 && event.charCode <= 57 || (event.charCode == 46)' title="Пример: 12.99. Максимум = 999.99" >
            </td>
        </tr>
        <tr>
                <td>
                К-во занятий:
            </td>
                <td>
                <input type="text" id="wellness" name="wellness" value="0" required pattern="[0-9]{1,2}" maxlength="2" minlength="1" onkeypress='return (event.charCode >=48 && event.charCode <= 57)' title="К-во тренировок. Максимум 99.">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><input type="submit" id="s" name="s" value="Регистрация"/></center>
            </td>
        </tr>
    </table>
</form:form>
    </div>
    <div id="footer">
        <div id="left">
            <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
        </div>
        <div id="right">
            <a href="/Mobileapp/magicnail/other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
            <a href="/Mobileapp/magicnail/exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
        </div>
    </div>
</body>

</html>
