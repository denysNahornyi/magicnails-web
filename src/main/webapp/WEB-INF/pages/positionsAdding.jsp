<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 6%;
            font-size: 100%;
            margin-bottom: 2%;
        }
        td {
            height: 40px;
            vertical-align: bottom;
            padding-bottom: 1.8%;
            /*padding-bottom: 20px;*/
            /*margin: 10px;*/
        }
    </style>
</head>
<body>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>
<form:form action="adding" method="post" acceptCharset="UTF-8" onsubmit="zapret()">
    <table cellspacing="1" align="center" >
        <tr>
            <td colspan="2">
                <center><h3>Добавление должности:</h3></center>
            </td>
        </tr>
        <tr>
            <td>
                Название должности:
            </td>
            <td>
                <input type="text"  id="name" name="name" required pattern="^[А-ЯЁ][а-яё\s\-]+$" title="Название должности русским языком. Первая буква заглавная." maxlength="30">
            </td>
        </tr>
        <tr>
            <td>
                Адрес:
            </td>
            <td align="center">
                <select id="adress" name="adress" required>
                    <c:forEach items="${adress}" var="adres">
                        <option value="${adres.id}">${adres.name}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><input type="submit" id="s" value="Ok"></center>
            </td>
        </tr>
    </table>
</form:form>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>


</body>
</html>
