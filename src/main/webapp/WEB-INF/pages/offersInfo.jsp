<%@ page import="com.project.magicMobileApp.crud.AdvertisingDAO" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="com.project.magicMobileApp.models.Advertising" %>
<%@ page import="com.project.magicMobileApp.models.Customer" %>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
    <style>
        table{
            margin-top: 1%;
            table-layout: fixed;
            width:90%;
            /*font-size: 80%;*/
            margin-bottom: 5%;
            border-collapse: collapse;
        }
        td {
            height: 40px;
            vertical-align: top;
            align-content: center;
            border: solid black 1px;
            word-wrap:break-word;
        }


        th{
            background: #ED9FC1;
            border: solid black 1px;
        }




    </style>
</head>
<body>
<div id="main">
<form:form action="/magicnail/clients/adding" method="post" acceptCharset=" UTF-8" >
    <table  align="center" >
        <tr>
            <th colspan="5" align="center">
                Рассылка:
            </th>
        </tr>
        <tr>
            <th align="center">
                Название
            </th>
            <th align="center">
                Текст
            </th>
            <th align="center">
                Начало
            </th>
            <th align="center">
                Конец
            </th>
            <th align="center">
                Получатели
            </th>
        </tr>
        <%
            AdvertisingDAO advertisingDAO = WebApplicationContextUtils.getWebApplicationContext(application).getBean(AdvertisingDAO.class);
            HashSet<String> name = advertisingDAO.getAll();
            for(String n: name){
        %>
        <tr>
            <td><%out.println(n);%></td>
            <%
                List<Advertising> advertising = advertisingDAO.getAdvertisingByName(n);
                String text = advertising.get(0).getText();
                List<Customer> customers1 = new ArrayList<>();
                for (Advertising a : advertising){
                    customers1.add(a.getCustomer());
             }
                Collections.sort(customers1, Customer.Comparators.LOGIN);
            %>
            <td><%out.println(text);%></td>
            <td align="center"><%
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd.MM.yyyy");
                String date1 = sdf.format(advertising.get(0).getDate1());
                String date2 = sdf.format(advertising.get(0).getDate2());
                out.println(date1);%></td>
            <td align="center"><%out.println(date2);%></td>
            <td>
               <p> <%
                    for (Customer c : customers1){
                        out.print(c.getCard() + " | " + c.getName()+" "+c.getSurname());
                        %>
                   </p>
                <%
                    }
                %>
            </td>
        </tr>
        <%
            }
        %>
    </table>
</form:form>
</div>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
