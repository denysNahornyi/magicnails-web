<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Magic Nails</title>

    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">

    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
<style>
    table{
    margin-top: 1%;
    font-size: 100%;
    margin-bottom: 2%;
        border-collapse: collapse;

    }
    td {
    height: 40px;
    vertical-align: center;
    padding-bottom: 1.8%;
        border: solid black 1px;
    }
    th{
        background: #ED9FC1;
        border: solid black 1px;
    }
</style>
</head>
    <body>
        <table align="center" >
            <tr>
                <th colspan="2">
                    Рейтинги мастеров
                </th>
            </tr>
            <c:forEach items="${rating}" var="r">
<tr >
<th >
${r.value.surname} ${r.value.name}
</th>
<th >
${r.value.position.name}
</th>
</tr>
<tr>
<td align="center" colspan="2">
${r.key}
</td>
</tr>
</c:forEach>
</table>
        <div id="footer">
            <div id="left">
                <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
            </div>
            <div id="right">
                <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
                <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
            </div>
        </div>
</body>
</html>
