<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 1%;
            font-size: 60%;
            margin-bottom: 2%;
        }
        td {
            height: 40px;
            vertical-align: center;
            padding-bottom: 1.8%;
        }
    </style>
</head>
<body onload="myFunction()">
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
    function CheckName(val) {
        var element=document.getElementById('b')
        if(val=='2')
            element.style.display='block'
        else
            element.style.display = 'none'
    }
    function myFunction() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }
        if(mm<10) {
            mm='0'+mm
        }
        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("data").value = today;
    }
</script>
<form:form action="adding" method="post" acceptCharset="UTF-8" onsubmit="zapret()">
    <table align="center" >
        <tr>
            <td colspan="3">
                <center><h3>Добавление проделаной услуги:</h3></center>
            </td>
        </tr>
        <tr>
            <td>
                Дата:
            </td>
            <td colspan="2">
                <center><input type="date" id="data" name="data" required></center>
            </td>
        </tr>
        <tr>
            <td>
                Имя клиента:
            </td>
            <td colspan="2">
                <center><select id="customerer" name="customerer" required>
                    <c:forEach items="${customers}" var="customer">
                        <option value="${customer.id}">${customer.card} | ${customer.surname} ${customer.name} - ${customer.bonuses} бонусов.</option>
                    </c:forEach>
                </select> </center>
            </td>
        </tr>
        <tr>
            <td>
                Имя мастера:
            </td>
            <td colspan="2">
                <center><select id="masterer" name="masterer" required>
                    <c:forEach items="${masters}" var="master">
                        <option value="${master.id}">${master.surname} ${master.name} ${master.position.name} ${master.position.adress.name}</option>
                    </c:forEach>
                </select> </center>
            </td>
        </tr>
        <tr>
            <td>
                Выбор услуг:
            </td>
            <td>
                <center>
                <select id="serviceer" name="serviceer" multiple required>
                    <optgroup label="Маникюр">
                        <c:forEach items="${services}" var="service">
                            <option value="${service.id}">${service.name}</option>
                        </c:forEach>
                    </optgroup>
                    <optgroup label="Бровки">
                        <c:forEach items="${brovki}" var="brov">
                            <option value="${brov.id}">${brov.name}</option>
                        </c:forEach>
                    </optgroup>
                    <optgroup label="Реснички">
                        <c:forEach items="${resni4ki}" var="res">
                            <option value="${res.id}">${res.name}</option>
                        </c:forEach>
                    </optgroup>
                </select>
                </center>
            </td>
            <td  style="text-align: center; font-size: 15px;" width="20px">
                Используйте Ctrl для выбора нескольких услуг.
            </td>
        </tr>
        <tr>
            <td>
                Ценник за дизайн:
            </td>
            <td colspan="2">
                <center><input  type="text" value="0" id="nailsPrice" name="nailsPrice" required pattern="^([0-9][0-9]{0,2}|1000)$" minlength="1" maxlength="4" title="Введите целое число от 0 до 1000"></center>
            </td>
        </tr>
        <tr>
            <td>
                Дополнительная скидка (%):
            </td>
            <td  colspan="2">
                <center><input  type="text" class="proc" value="0" id="proc" name="proc" required pattern="^\d{1,2}|100$" minlength="1" maxlength="3"></center>
            </td>
        </tr>
        <tr>
            <td>
                Списаны ли бонусы?
            </td>
            <td colspan="2">
                <center>
                <input type="radio" name="bonuces" value="1" checked onclick="CheckName(this.value)"> Да<br>
                <input type="radio" name="bonuces" value="0" onclick="CheckName(this.value)"> Нет<br>
                <input type="radio" name="bonuces" value="2" onclick="CheckName(this.value)"> Частично
                </center>
            </td>
        </tr>

        <tr>

            <td colspan="3" >
                <center><input type="text" style="display: none ;" value="0" id="b" name="b" required pattern="^[0-9]{1,3}[.]{0,1}[0-9]{0,2}"  maxlength="6" minlength="1" onkeypress='return event.charCode >=48 && event.charCode <= 57 || (event.charCode == 46)' title="Пример: 12.99. Максимум = 999.99" ></center>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <center><input type="submit" id="s" value="Все верно"></center>
            </td>
        </tr>
    </table>
</form:form>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
