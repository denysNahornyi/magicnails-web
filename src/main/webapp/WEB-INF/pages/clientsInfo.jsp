<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
    <style>
        table{
            margin-top: 1%;
            margin-bottom: 5%;
            padding: 10px;
            border-collapse: collapse;
        }
        td {
            height: 40px;
            vertical-align: bottom;
            border: solid black 1px;
        }
        th{
            background: #ED9FC1;
            border: solid black 1px;
        }
    </style>
</head>
<div id="main">
    <table align="center" cellpadding="5" >
<tr>
    <th colspan="10" align="center">
        Клиентская база:
    </th>
</tr>
<tr>
    <th align="center">
        Номер карты
    </th>
    <th align="center">
        ФИО
    </th>
    <th align="center">
        Дата рождения
    </th>
    <th align="center">
        Номер мобильного
    </th>
    <th align="center">
        Логин
    </th>
    <th align="center">
        Пароль
    </th>
    <th align="center">
        К-во бонусов
    </th>
    <th align="center">
        К-во тренеровок
    </th>
    <th align="center">
        Дата входа
    </th>
    <th align="center">
        Фото
    </th>
</tr>
        <c:forEach items="${customers}" var="customer">
            <tr>
                <td align="center">
                        ${customer.card}
                </td>
                <td align="center">
                        ${customer.surname} ${customer.name} ${customer.patronymic}
                </td>
                <td align="center">
                        ${customer.date}
                </td>
                <td align="center">
                        ${customer.phone}
                </td>
                <td align="center">
                        ${customer.login}
                </td>
                <td align="center">
                        ${customer.password}
                </td>
                <td align="center">
                        ${customer.bonuses}
                </td>
                <td align="center">
                        ${customer.wellness}
                </td>


                <c:choose>
                    <c:when test="${not empty customer.lastUse}">
                <%--<td align="center" bgcolor= #880E4F style="color: white">--%>
                        <td align="center">
                        ${customer.lastUse.date}.${customer.lastUse.month+1}.${customer.lastUse.year+1900}
                </td>
                </c:when>
                <c:otherwise>
                 <td></td>
                </c:otherwise>
                </c:choose>
                <td align="center">
                    <c:if test="${customer.img!=null}">
                    <a style="cursor: pointer" ONCLICK="window.open('/Mobileapp/magicnail/image/${customer.id}','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=800');">Показать фото.</A>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"><img class="img"  src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img"  src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
