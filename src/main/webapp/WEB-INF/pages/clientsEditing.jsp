<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Редактирование информации</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 6%;
            font-size: 100%;
        }
        td {
            height: 40px;
            vertical-align: bottom;
        }
    </style>
</head>
<body>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
        var del = document.getElementById('d').disabled = true;
    }
    function  getRequest() {
        var form = document.getElementById("user")|| null
        if(form){
            var sel = document.getElementById('id');
            var index = sel.options[sel.selectedIndex].value;
            form.action = "deleting/"+index
            form.method= "post"
            form.submit()
        }
    }
</script>
<div id="main">
<form:form action="editing" method="post" acceptCharset=" UTF-8" onsubmit="zapret()" >
    <table cellspacing="1" align="center" >
        <tr>
            <td colspan="2">
                <center><h3>Редактирование клиента</h3></center>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top">
                Выбор клиента:
            </td>
            <td>
                <select id="id" name="id" title="Выберите клиента" onmousedown="if(this.options.length>10){this.size=10;}"  onchange='this.size=0;' onblur="this.size=0;" required>
                    <c:forEach items="${customers}" var="customer">
                        <option value="${customer.id}">${customer.card} | ${customer.surname} ${customer.name} </option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <center><input type="submit" id="s" value="Редактировать"></center>
            </td>
            </form:form>
            <form:form id="user" acceptCharset=" UTF-8" >
            <td width="100px">
                <center><input id="d" onclick="getRequest(); zapret()" type="submit" value="Удалить"></center>
            </td>
            </form:form>
        </tr>
    </table>
</div>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
