<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Главная страница</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">

    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">

    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
</head>
<body>
<script>
    function today() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }
        if(mm<10) {
            mm='0'+mm
        }
        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("href").href="/Mobileapp/magicnail/services/info/"+today;
    }
</script>






<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="MN">MagicNails</div>
            <div id="text">Предоставленые услуги</div>
            <div class="col-md-6 .col-md-offset-3">
                <ul class="soc">
                    <li class="serv"><a href="services/adding"><div class="x">Новая</div><div class="n">услуга</div></a></li>
                    <li class="prices"><a href="services/deleting">Удаление</a></li>
                    <li class="offers"><a href="appointment/adding"><div class="x">Запись</div><div class="n">клиента</div></a></li>
                    <li class="services"><a href="#" id="href" onclick="today()">Отчет</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <div id="left">
        <a href="main">Главная</a> | <a href="clients">Клиенты</a> |  <a href="services">Услуги</a> | <a href="offers">Рассылка</a> | <a href="prices">Цены</a> | <a href="wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>

</body>
</html>
