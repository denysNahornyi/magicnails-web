<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 6%;
            font-size: 100%;
            margin-bottom: 2%;
        }
        td {
            height: 40px;
            vertical-align: bottom;
            padding-bottom: 1.8%;
        }

        #amount{
            width: 14%;
        }
    </style>
</head>
<body>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>

<form:form action="services" method="post" acceptCharset="UTF-8" onsubmit="zapret()">
    <table cellspacing="1" align="center" >
        <tr>
            <td colspan="2">
                <center><h3>Редактирование к-ва занятий в остатке клинта:</h3></center>
            </td>
        </tr>
        <tr>
            <td>
                Выбор клинта
            </td>
            <td align="center">
                <select id="id" name="id">
                    <c:forEach items="${customers}" var="service">
                        <option value="${service.id}">${service.card} | ${service.surname} ${service.name}  - ${service.wellness} занятий.</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                К-во занятий
            </td>
            <td align="center">
                <input type="text"  id="amount" name="amount" required pattern="[0-9]{1,2}" maxlength="2" minlength="1" title="К-во тренировок. Максимум 99.">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <input type="submit" id="s" value="Верно"/>
            </td>
        </tr>
    </table>
</form:form>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
