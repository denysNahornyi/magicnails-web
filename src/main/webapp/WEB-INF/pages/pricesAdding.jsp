<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
    <style>
        table{
            margin-top: 1%;
            margin-bottom: 5%;
            padding: 10px;
            font-size: 100%;
        }

        td {
            padding-bottom: 1.8%;
            height: 40px;
            vertical-align: center;
            height: 40px;
        }
    </style>
</head>
<body>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>
<div id="main">
<form:form action="adding" method="post" acceptCharset="UTF-8" onsubmit="zapret()">
    <table cellspacing="1" align="center" >
        <tr>
            <td colspan="2">
                <center><h3>Добавление услуги и ее цены:</h3></center>
            </td>
        </tr>
        <tr>
            <td>
                Отрасль услуги:
            </td>
            <td>
                <div style="margin-left: 20%">
                    <input type="radio" name="param" value="1" checked> Маникюр<br>
                    <input type="radio" name="param" value="0"> Бровки<br>
                    <input type="radio" name="param" value="0"> Реснички<br>
                </div>
            </td>
        </tr>
        <tr>
            <td >
                Название услуги:
            </td>
            <td>
                <input type="text"  id="name" name="name" required pattern="^[А-ЯЁ][а-яё\s\-]+$" title="Название услуги русским языком. Первая буква заглавная." maxlength="30">
            </td>
        </tr>
        <tr>
            <td>
                Ее цена:
            </td>
            <td>
                <input type="text"  id="price" name="price" required pattern="^([1-9][0-9]{0,2}|1000)$">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center> <input type="submit" id="s" value="Все верно"></center>
            </td>
        </tr>
    </table>
</form:form>
</div>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
