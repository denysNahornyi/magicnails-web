<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 6%;
            font-size: 100%;
            margin-bottom: 2%;
        }
        td {
            height: 40px;
            vertical-align: center;
            padding-bottom: 1.8%;
        }



    </style>
</head>
<body>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>
<form:form action="price" method="post" acceptCharset=" UTF-8" onsubmit="zapret()" >
    <table cellspacing="1" align="center" >
    <tr>
        <td colspan="2">
            <center><h3>Редактирование цен на Веллнесс:</h3></center>
        </td>
    </tr>
    <tr>
        <td>
            Цена за разовое занятие:
        </td>
        <td>
        <input type="text" id="priceFor1Training" name="priceFor1Training" value="${price.priceFor1Training}"  required pattern="^([1-9][0-9]{0,2}|1000)$">
        </td>
    </tr>
    <tr>
    <td>
    Цена за пробное занятие:
    </td>
        <td>
    <input type="text" id="testTraining" name="testTraining" value="${price.testTraining}"  required pattern="^([1-9][0-9]{0,2}|1000)$">
    </td>
    </tr>
    <tr>
    <td>
    Цена первого абонемента:
    </td>
        <td>
    <input type="text" id="priceForFirstMonth" name="priceForFirstMonth" value="${price.priceForFirstMonth}"  required pattern="^([1-9][0-9]{0,2}|1000)$">
    </td>
    </tr>
    <tr>
    <td>
    Цена последующих абонементов:
    </td>
        <td>
    <input type="text" id="priceForSecondMonth" name="priceForSecondMonth" value="${price.priceForSecondMonth}"  required pattern="^([1-9][0-9]{0,2}|1000)$">
    </td>
    </tr>
    <tr>
        <td align="center" colspan = "2">
            <input type="submit"  id="s" value="Редактировать">
        </td>
    </tr>
</table>
    </form:form>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img"  src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
