<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 1%;
            font-size: 100%;
        }
        td {
            height: 40px;
            vertical-align: bottom;
            padding-bottom: 1.8%;
        }
    </style>
</head>
<body>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>
<form:form action="editing" method="post" acceptCharset=" UTF-8" onsubmit="zapret()" >
    <table cellspacing="1" align="center" >
        <tr>
            <td colspan="2">
                <center><h3>Редакторование информации о салоне</h3></center>
            </td>
        </tr>
        <%--<tr>--%>
            <%--<td>--%>
                <%--Название--%>
            <%--</td>--%>
            <%--<td>--%>
                <%--<input type="text" id="name" name="name" value="${saloon.name}">--%>
            <%--</td>--%>
        <%--</tr>--%>
        <tr>
            <td>
                Мобильный номер №1:
            </td>
            <td width="100px"  >
                <input type="text" id="mobileNumber1" minlength="13" name="mobileNumber1" value="${saloon.mobileNumber1}">
            </td>
        </tr>
        <tr>
            <td>
                Мобильный номер №2:
            </td>
            <td>
                <input type="text" id="mobileNumber2" minlength="13" name="mobileNumber2" value="${saloon.mobileNumber2}">
            </td>
        </tr>
        <tr>
            <td>
                Instagram:
            </td>
            <td>
                <input type="text" id="insta" name="insta" value="${saloon.insta}">
            </td>
        </tr>
        <%--<tr>--%>
            <%--<td>--%>
                <%--Ссылка на сайт маникюра:--%>
            <%--</td>--%>
            <%--<td>--%>
                <%--<input type="text" id="web1" name="web1" value="${saloon.web1}" >--%>
            <%--</td>--%>
        <%--</tr>--%>
        <%--<tr>--%>
            <%--<td>--%>
                <%--Ссылка на сайт веллнесса:--%>
            <%--</td>--%>
            <%--<td>--%>
                <%--<input type="text" id="web2" name="web2" value="${saloon.web2}" >--%>
            <%--</td>--%>
        <%--</tr>--%>
        <tr>
            <td>
                Ссылка на запись на маникюр:
            </td>
            <td>
                <input type="text" id="reg1" name="reg1" value="${saloon.reg1}">
            </td>
        </tr>
        <tr>
            <td>
                Ссылка на запись на веллнесс:
            </td>
            <td>
                <input type="text" id="reg2" name="reg2" value="${saloon.reg2}">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><input type="submit" id="s" value="Все верно"/></center>
            </td>
        </tr>
    </table>
</form:form>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>

