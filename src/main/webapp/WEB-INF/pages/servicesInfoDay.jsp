<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <html>
        <head>
            <title>Magic Nails</title>
            <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
            <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
            <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
            <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
            <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
            <style>
                table{
                    margin-top: 1%;
                    width: 100%;
                    margin-bottom: 5%;
                    padding: 10px;
                    border-collapse: collapse;
                    font-size: 100%;
                }
                td {
                    height: 40px;
                    vertical-align: bottom;
                    border: solid black 1px;
                }

                th {
                    background: #ED9FC1;
                    border: solid black 1px;
                }


            </style>
        </head>
<body onload="myFunction()">
<script>
    function  getDate() {
        var date = document.getElementById('d').value;
        document.location.href = "/Mobileapp/magicnail/services/info/"+date;
    }
    function myFunction() {
        document.getElementById("d").value = '${cal}';
    }
</script>
    <table  align="center">
        <tr>
            <th colspan="7" align="center">
                История услуг за ${str}:
            </th>
        </tr>
        <tr>
            <td colspan="2">
                Выбор даты:
            </td>
            <td colspan="3">
                <center><input style="margin-bottom: 3px" type="date" id="d" required onchange="getDate()"></center>
            </td>
        </tr>
    <c:choose>
        <c:when test="${empty history}">
        <tr>
            <td align="center" width="100%" colspan="7">
                История за данное число отсутствует.
            </td>

        </tr>
    </c:when>
        <c:otherwise>
        <tr>
            <th align="center">
                Дата
            </th>
            <th align="center">
                Клиент
            </th>
            <th align="center">
                Услуга
            </th>
            <th align="center">
                Мастер
            </th>
            <th align="center">
                Рейтинг
            </th>
        </tr>
        <c:forEach items="${history}" var="h">
            <tr>
                <td align="center">
                        ${h.date.date}.${h.date.month+1}.${h.date.year+1900}
                </td>
                <td align="center">
                        ${h.customer.surname} ${h.customer.name}
                </td>
                <td align="center">
                        ${h.service.name}
                </td>
                <td align="center">
                        ${h.master.surname} ${h.master.name}
                </td>
                <td align="center" >
                        ${h.rating}
                </td>
            </tr>
        </c:forEach>
        </c:otherwise>
        </c:choose>
    </table>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>

