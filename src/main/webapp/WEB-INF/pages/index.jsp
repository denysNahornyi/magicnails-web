<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Magic Nails</title>
        <link href="<c:url value="/resources/css/css.css" />" rel="stylesheet">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    </head>
    <body>
<div class="main">
    <div class="container">
        <center>
            <div class="middle">
                <div id="login">
                    <form action="login"  method="post">
                        <fieldset class="clearfix">
                            <p ><span class="fa fa-user"></span><input type="text"  name="username" placeholder="Логин" required></p>
                            <p><span class="fa fa-lock"></span><input type="password"  name="password" placeholder="Пароль" required></p>
                            <div>
                                <span style="width:50%;   display: inline-block;"><input type="submit" value="Вход"></span>
                            </div>
                        </fieldset>
                        <div class="clearfix"></div>
                    </form>
                </div>
                <div class="logo"><img width="200px" height="200px" src="<c:url value="/resources/img/x.jpg"/>" alt="LOGO">
                </div>
            </div>
        </center>
    </div>
</div>
</body>
</html>
























