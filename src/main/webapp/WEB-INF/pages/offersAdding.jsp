<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">

    <style>
        table{
            margin-top: 1%;
            font-size: 100%;
            margin-bottom: 2%;

        }
        td {
            height: 40px;
            vertical-align: center;
            padding-bottom: 1.8%;
        }


        input[type="date"]
        {
            font-family: 'Caviar Dreams';
            font-size: 60%;
            border-radius:4px;
            -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            background: #f8f8f8;
            color:#888;
            margin-left: 30px;
            width: 67%;
        }

        /**/

        /*.singleMultiple {*/
            /*width: 100%;*/
            /*height: 100px;*/
        /*}*/

    </style>
</head>
<script>
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
    }
</script>
<div id="main">
<form:form action="adding" method="post" acceptCharset="UTF-8" onsubmit="zapret()">
    <table cellspacing="1" align="center" >
        <tr>
            <td colspan="3">
                <center><h3>Персональное предложение:</h3></center>
            </td>
        </tr>
        <tr>
            <td>
                Оглавление:
            </td>
            <td  colspan="2">
                <input type="text"  id="name" name="name" required maxlength="250">
            </td>
        </tr>
        <tr>
            <td>
                Текст:
            </td>
            <td  colspan="2" >
                <input type="text"  id="taxt" name="taxt" required maxlength="250">
            </td>
        </tr>
        <tr>
            <td width="100px"  >
                Выбор клиентов:
            </td>
            <td width="100px"  >
                <select id="customerer" class="singleMultiple" name="customerer"  multiple="multiple" required>
                    <c:forEach items="${customers}" var="customer">
                        <option value="${customer.id}">${customer.card} | ${customer.surname} ${customer.name}</option>
                    </c:forEach>
                </select>
            </td>
            <td  style="text-align: center; font-size: 15px;" width="20px">
                Используйте Ctrl для выбора нескольких услуг.
            </td>
        </tr>
        <tr>
            <td>
                Начало акции:
            </td>
            <td colspan="2" >
                <input type="date" name="data1" required >
            </td>
        </tr>
        <tr>
            <td >
                Конец акции:
            </td>
            <td  colspan="2">
                <input type="date" name="data2" required>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <center><input type="submit" id="s" name="s"  value="Опубликовать"></center>
            </td>
        </tr>
    </table>
</form:form>
</div>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp/magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp/magicnail/services">Услуги</a> | <a href="/Mobileapp/magicnail/offers">Рассылка</a> | <a href="/Mobileapp/magicnail/prices">Цены</a> | <a href="/Mobileapp/magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp/magicnail/other"> <img class="img" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp/magicnail/exit"><img class="img" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
