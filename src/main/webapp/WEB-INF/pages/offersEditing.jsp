<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
    <style>
        table{
            margin-top: 1%;
            font-size: 100%;
            margin-bottom: 2%;
        }
        td {
            height: 40px;
            vertical-align: bottom;
            padding-bottom: 1.8%;
        }



        input[type="date"]
        {
            font-family: 'Caviar Dreams';
            font-size: 60%;
            border-radius:4px;
            -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            background: #f8f8f8;
            color:#888;
            margin-left: 30px;
            width: 94%;
        }


        input[type="text"]
        {
            font-family: 'Caviar Dreams';
            font-size: 60%;
            text-align: center;
            margin-left: 30px;
            border-radius:4px;
            -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
            background: #f8f8f8;
            color:#888;
            width: 94%;
        }

    </style>
</head>
<body>
<script>
    function  getRequest() {
        var form = document.getElementById("user")|| null
        if(form){
            var sel = document.getElementById('name');
            var index = sel.options[sel.selectedIndex].value;
            form.action = "deleting/"+index
            form.method= "post"
            form.submit()
        }
    }
    function CheckName(val) {
        var element=document.getElementById('x')
        if(val=='1')
            element.style.display='block'

        else
            element.style.display = 'none'
    }
    function zapret()
    {
        var btn = document.getElementById('s').disabled = true;
        var del = document.getElementById('d').disabled = true;
    }
</script>

<form:form action="editing" onsubmit="zapret()">



<table cellspacing="1" align="center" >
    <tr>
        <td colspan="2">
            <center><h3>Редактирование:</h3></center>
        </td>
    </tr>
    <tr>
        <td>
          Выбор предложения
        </td>
        <td >
            <select id="name" name="name">
                <c:forEach items="${offers}" var="offer">
                    <option value="${offer}">${offer}</option>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            Новое название:
        </td>
        <td>
            <input type="text"  id="newName" name="newName" required maxlength="250">
        </td>
    </tr>
    <tr>
        <td>
            Новый текст:
        </td>
        <td>
            <input type="text"  id="newText" name="newText" required maxlength="250">
        </td>
    </tr>
    <tr>
        <td>
            Начало:
        </td>
        <td>
            <input type="date" name="data1" required>
        </td>
    </tr>
    <tr>
        <td>
           Конец:
        </td>
        <td>
            <input type="date" name="data2" required>
        </td>
    </tr>
    <tr>
        <td>
           <center> <input type="submit" id="s" value="Редактировать"></center>
        </td>
        </form:form>
        <form:form id="user" acceptCharset=" UTF-8" >
            <td width="100px" align="center">
                <input onclick="getRequest(); zapret()" type="submit" id="d" value="Удалить">
            </td>
        </form:form>
    </tr>
</table>
<div id="footer">
    <div id="left">
        <a href="/Mobileapp//magicnail/main">Главная</a> | <a href="/Mobileapp/magicnail/clients">Клиенты</a> |  <a href="/Mobileapp//magicnail/services">Услуги</a> | <a href="/Mobileapp//magicnail/offers">Рассылка</a> | <a href="/Mobileapp//magicnail/prices">Цены</a> | <a href="/Mobileapp//magicnail/wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="/Mobileapp//magicnail/other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="/Mobileapp//magicnail/exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>





