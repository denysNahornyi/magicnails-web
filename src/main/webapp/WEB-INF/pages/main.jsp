<%@ page import="java.util.Date" %>
<%@ page import="com.project.magicMobileApp.crud.SaloonDAO" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="com.project.magicMobileApp.models.Saloon" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Magic Nails</title>
    <link href="<c:url value="/resources/css/cssMain.css" />" rel="stylesheet">
    <link href='http://webfonts.ru/import/caviar.css' rel="stylesheet" type="text/css">
    <link href='http://webfonts.ru/import/stark.css' rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Monofett" rel="stylesheet">
    <link href='http://webfonts.ru/import/baronneue.css' rel="stylesheet" type="text/css">
    <link rel="icon" type="image/x-icon"  href="<c:url value="/resources/img/logo.ico"/>">
</head>
<div id="main">
<script>
    function today() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }
        if(mm<10) {
            mm='0'+mm
        }
        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("href").href="/Mobileapp/magicnail/dbRemoving/"+today;
    }
</script>
<%
    Date today = new Date();
    SaloonDAO saloonDAO = WebApplicationContextUtils.getWebApplicationContext(application).getBean(SaloonDAO.class);
    Saloon saloon = saloonDAO.get(1);
    Date year = saloon.getDbRemove();
    long raznica = today.getTime() - year.getTime();
    session.setAttribute("raznica", raznica);
%>
<c:if test="${raznica>31536000000}">
    <h1 style="color: red">Очистите базу!</h1>
    <a href="#" id="href" onclick="today()"><button>Очистка базы</button></a>
</c:if>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="MN">MagicNails</div>
            <div id="text"> Главная страница</div>
            <div class="col-md-6 .col-md-offset-3">
                <ul class="social">
                    <li class="clients"><a href="clients">Клиенты</a></li>
                    <li class="services"><a href="services">Услуги</a></li>
                    <li class="offers"><a href="offers">Акции</a></li>
                    <li class="prices"><a href="prices">Цены</a></li>
                    <li class="fifth"><a href="wellness">Wellness</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<div id="footer">
    <div id="left">
    <a href="main">Главная</a> | <a href="clients">Клиенты</a> |  <a href="services">Услуги</a> | <a href="offers">Рассылка</a> | <a href="prices">Цены</a> | <a href="wellness">Веллнесс</a>
    </div>
    <div id="right">
        <a href="other"> <img height="24px" width="24px" src="<c:url value="/resources/img/set.png"/>"></a>
        <a href="exit"><img height="24px" width="24px" src="<c:url value="/resources/img/door.png"/>"></a>
    </div>
</div>
</body>
</html>
